package de.dksc0der.cbb.web.validation;

import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import de.dksc0der.cbb.repository.MemberSecurityService;

@FacesValidator("emailValidator")
public class EmailValidator implements Validator {

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	private ResourceBundle bundle = null;
	
	@EJB
	private MemberSecurityService securityService;
	
	private Pattern pattern;
	private Matcher matcher;

	public EmailValidator() {
		pattern = Pattern.compile(EMAIL_PATTERN);
	}
	
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		String email = value.toString();
		
		
		bundle = ResourceBundle.getBundle("languages.language", context.getViewRoot().getLocale());
		matcher = pattern.matcher(email);
		
		if(!matcher.matches()){
			FacesMessage msg = new FacesMessage(bundle.getString("validator.email.error"), bundle.getString("validator.email.error.format"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);

		}else if(securityService.isMemberExistByEmail(email)){
			FacesMessage msg = new FacesMessage(bundle.getString("validator.email.error"), bundle.getString("validator.email.error.exist"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}else{
			FacesMessage msg = new FacesMessage(bundle.getString("validator.email.success"), bundle.getString("validator.email.success.info"));
			msg.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage("input_form:email", msg);
		}
	}

}
