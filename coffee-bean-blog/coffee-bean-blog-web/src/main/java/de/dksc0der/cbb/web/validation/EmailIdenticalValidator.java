package de.dksc0der.cbb.web.validation;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletRequest;

@FacesValidator("emailIdenticalValidator")
public class EmailIdenticalValidator implements Validator {
	
	private ResourceBundle bundle = null;
	
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		bundle = ResourceBundle.getBundle("languages.language", context.getViewRoot().getLocale());
		HttpServletRequest param = (HttpServletRequest)context.getExternalContext().getRequest();
		String email = param.getParameter("input_input_form:email");
		String confirm_email = (String)value;
		
		
		if(!confirm_email.equals(email)){
			FacesMessage msg = new FacesMessage(bundle.getString("validator.email.confirm.error"), bundle.getString("validator.email.confirm.error.info"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);

		}else{
			FacesMessage msg = new FacesMessage(bundle.getString("validator.email.confirm.success"), bundle.getString("validator.email.confirm.success.info"));
			msg.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage("input_form:confirm_email", msg);
		}
	}

}
