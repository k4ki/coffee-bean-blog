package de.dksc0der.cbb.web.validation;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletRequest;

@FacesValidator("passwordIdenticalValidator")
public class PasswordIdenticalValidator implements Validator {
	private ResourceBundle bundle = null;

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		bundle = ResourceBundle.getBundle("languages.language", context.getViewRoot().getLocale());
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		String password = request.getParameter("input_form:password");
		System.out.println(password + request.getParameterMap());
		String confirm_password = (String) value;

		if (!confirm_password.equals(password)) {
			FacesMessage msg = new FacesMessage(bundle.getString("validator.password.confirm.error"), bundle.getString("validator.password.confirm.error.info"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);

		} else {
			FacesMessage msg = new FacesMessage(bundle.getString("validator.password.confirm.success"), bundle.getString("validator.password.confirm.success.info"));
			msg.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage("form:confirm_password", msg);
		}
	}

}
