package de.dksc0der.cbb.web.listener;

import java.sql.Timestamp;
import java.util.concurrent.Callable;

import javax.annotation.security.RunAs;
import javax.ejb.EJB;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.jboss.logging.Logger;

import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.repository.MemberSecurityService;
import de.dksc0der.cbb.util.AppConstants;
import de.dksc0der.cbb.util.role.MemberVerified;

@RunAs(AppConstants.ROLE_MEMBER_VERIFIED)
public class SessionListener implements HttpSessionListener {

	private static Logger log = Logger.getLogger(SessionListener.class.getName());

	@EJB
	private MemberSecurityService securityServcie;
	
	@EJB
	private MemberVerified memberVerified;

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		log.info("Session created on " + (new Timestamp(se.getSession().getCreationTime())) + " with sessionid "
				+ se.getSession().getId());
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {

		try {

			memberVerified.call(new Callable<Member>() {

				@Override
				public Member call() throws Exception {

					Member member = null;

					member = (Member) se.getSession().getAttribute("member");

					if (member != null){
						
						member = securityServcie.endSession(member);
						
						
					}
					log.info("Automatic logged out Member -->> \n" + se.getSession().getAttribute("member"));

					return null;
				}

			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
