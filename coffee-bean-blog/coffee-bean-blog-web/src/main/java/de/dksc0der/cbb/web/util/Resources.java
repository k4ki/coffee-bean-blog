package de.dksc0der.cbb.web.util;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;

public class Resources {
	/**
	 * Produce with the requestscoped the current instance from FacesContext
	 * 
	 * @return FacesContext current instance
	 */
	@Produces
	@RequestScoped
	public FacesContext produceFacesContext(){
		return FacesContext.getCurrentInstance();
	}
	
	/**
	 * Static method for determinig the lowest level error message of an Exception Chain.
	 * 
	 * @param e Exception caught
	 * @param Lowest level error message
	 * 
	 */
	public static synchronized String getRootErrorMessage(Exception e){
		// Default to general error message that registration failed.
		String errorMessage = "Sevre Error. See server log for more information";
		if(e == null){
			// This shouldn´t happen, but return the default messages
			return errorMessage;
		}
		// Start with the exception and recurse to find the root cause
		Throwable t = e;
		while (t != null) {
			// Get the message from the Throwable class instance
			errorMessage = t.getLocalizedMessage();
			t = t.getCause();
		}
		// This is the root cause message
		return errorMessage;
	}
}
