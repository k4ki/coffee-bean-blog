package de.dksc0der.cbb.web.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import de.dksc0der.cbb.model.Category;
import de.dksc0der.cbb.model.Post;
import de.dksc0der.cbb.repository.CategoryService;
import de.dksc0der.cbb.repository.MemberPostService;

@ViewScoped
@ManagedBean
public class IndexController implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private FacesContext facesContext;

	@EJB
	private MemberPostService postService;

	@EJB
	private CategoryService categoryService;

	private List<Post> posts;
	private List<Category> categories;

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public void onload() {
		String[] verified = (String[]) facesContext.getExternalContext().getSessionMap().remove("verified");

		if (verified != null) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Herzlichen Glückwunsch",
					"Sie sind jetzt voll verifiziert für Coffee-Bean-Blog!!! Bitte melden Sie sich erneut an...");
			facesContext.addMessage(null, msg);
		}
		System.out.println("indexcontroller onload");
		posts = new CopyOnWriteArrayList<Post>(postService.findAll());
		categories = new CopyOnWriteArrayList<Category>(categoryService.findAll());
	}

	public void postsByCategory(Category category) {
		CopyOnWriteArrayList<Post> posts = new CopyOnWriteArrayList<Post>();

		Iterator<Post>itrPost = postService.findAll().iterator();
		while (itrPost.hasNext()) {
			Post post = (Post) itrPost.next();
			Iterator<Category>itrCategories = post.getCategories().iterator();
			while (itrCategories.hasNext()) {
				Category postCategory = (Category) itrCategories.next();
				if(postCategory.getCategoryname().equals(category.getCategoryname())){
					posts.add(post);
				}
			}
		}
		System.out.println(posts);
		this.setPosts(posts);
	}

	public void onSelect(Post post, String typeOfSelection, String indexes) throws IOException {
        System.out.println("OnSelect:" + post + " typeOfSelection: " + typeOfSelection + " indexes: " + indexes);

        facesContext.getExternalContext().redirect("blog/post-view.xhtml?id=" + post.getId());
    }
     
    public void onDeselect(Post post, String typeOfSelection, String indexes) {
        System.out.println("OnDeselect:" + post + " typeOfSelection: " + typeOfSelection + " indexes: " + indexes);
        
    }

}
