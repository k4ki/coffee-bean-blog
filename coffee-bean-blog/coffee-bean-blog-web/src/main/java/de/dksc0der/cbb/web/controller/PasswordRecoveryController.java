package de.dksc0der.cbb.web.controller;

import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.repository.MemberSecurityService;
import de.dksc0der.cbb.util.email.EmailTemplateBuilder;
import de.dksc0der.cbb.util.email.types.EmailType;

@ViewScoped
@ManagedBean
public class PasswordRecoveryController {
	
	@Resource(name="DefaultMangedExecutorService")
	private ManagedExecutorService executor;
	
	@Inject
	private FacesContext facesContext;
	
	@EJB
	private MemberSecurityService securityService;

	private ResourceBundle bundle;
	
	private EmailTemplateBuilder emailBuilder;
	
	private String email;
	private String confirmEmail;
	
	private String password;
	private String confirmPassword;
	
	private boolean begin;
	private boolean end;
	private boolean edit;
	
	private String token;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getConfirmEmail() {
		return confirmEmail;
	}
	public void setConfirmEmail(String confirmEmail) {
		this.confirmEmail = confirmEmail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public boolean isBegin() {
		return begin;
	}
	public void setBegin(boolean begin) {
		this.begin = begin;
	}
	public boolean isEnd() {
		return end;
	}
	public void setEnd(boolean end) {
		this.end = end;
	}
	
	public boolean isEdit() {
		return edit;
	}
	public void setEdit(boolean edit) {
		this.edit = edit;
	}
	@PostConstruct
	private void initialize(){
		bundle = ResourceBundle.getBundle("languages/language", facesContext.getViewRoot().getLocale());
		emailBuilder = new EmailTemplateBuilder(facesContext.getViewRoot().getLocale());
		token = ((HttpServletRequest)facesContext.getExternalContext().getRequest()).getParameter("checksum");
		
		if(token != null){
			this.setEdit(true);
		}else{
			this.setBegin(true);
		}
		
		this.setEnd(false);
	}
	
	@PermitAll
	public void sendMail() throws Exception{
		String token = securityService.passwordReset(email);
		
		if(token == null || token.equals("")){
			FacesMessage msg = new FacesMessage("Fehler", "Serverfehler...");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
		
		String emailBodyText = emailBuilder.createEmailBodyByTemplate(EmailType.PASSWORD_RECOVERY, token, facesContext.getViewRoot().getLocale());
		String subject = emailBuilder.createEmailSubject(EmailType.PASSWORD_RECOVERY, facesContext.getViewRoot().getLocale());
		
		sendmail(email, "zus3iik4k@gmail.com", subject, emailBodyText);
		
		
		this.setBegin(false);
		this.setEnd(true);
	}
	
	@PermitAll
	public void editPassword(){
		Member member = securityService.passwordUpdate(token, password);
		
		if(member  != null){
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, bundle.getString("verificator.controller.success"), bundle.getString("verificator.controller.success.email.edit"));
			facesContext.addMessage(null, msg);
			
		}else{
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, bundle.getString("verificator.controller.error"), bundle.getString("verificator.controller.error.email.edit"));
			facesContext.addMessage(null, msg);
		}
	}
	
	@PermitAll
	public void sendmail(String to, String from, String subject, String body){
		Runnable task = new Runnable() {
			
			@Override
			public void run() {
				Future<Boolean>result = emailBuilder.sender(to, from, subject, body, true);
				
				try {
					result.get();
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
			}
		};
		
		executor.submit(task);
	}
}
