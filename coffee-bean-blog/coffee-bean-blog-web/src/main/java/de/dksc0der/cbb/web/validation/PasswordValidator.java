package de.dksc0der.cbb.web.validation;

import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("passwordValidator")
public class PasswordValidator implements Validator {

	private static final String EMAIL_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!\"§()@#$%^&+*;:.,=])(?=\\S+$).{8,}$";
	
	private ResourceBundle bundle = null;

	private Pattern pattern;
	private Matcher matcher;

	public PasswordValidator() {
		pattern = Pattern.compile(EMAIL_PATTERN);
	}
	
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		String password = value.toString();
		
		
		bundle = ResourceBundle.getBundle("languages.language", context.getViewRoot().getLocale());
		matcher = pattern.matcher(password);
		
		if(!matcher.matches()){
			FacesMessage msg = new FacesMessage(bundle.getString("validator.password.error"), bundle.getString("validator.password.error.info"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);

		}else{
			FacesMessage msg = new FacesMessage(bundle.getString("validator.password.success"), bundle.getString("validator.password.success.info"));
			msg.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage("form:password", msg);
		}
	}

}
