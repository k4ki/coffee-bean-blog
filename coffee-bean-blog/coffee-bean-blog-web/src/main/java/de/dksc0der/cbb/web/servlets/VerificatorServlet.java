package de.dksc0der.cbb.web.servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.repository.MemberSecurityService;

/**
 * Servlet implementation class VerificatorServlet
 */
@WebServlet("/verified-account")
public class VerificatorServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	@EJB
	private MemberSecurityService securityService;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public VerificatorServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Member member = (Member)request.getSession().getAttribute("member");
		
		
		String token = request.getParameter("checksum");
		member = securityService.endVerification(token);
		
		member = securityService.endSession(member);
		
		request.logout();
		request.getSession().invalidate();
		
		String[] verified = new String[2];
		if(member != null){
			verified[0] = "Herzlichen Glückwunsch";
			verified[1] = "Sie sind jetzt voll verifiziert für Coffee-Bean-Blog!!! Bitte melden Sie sich erneut an...";
		
		}else{
			verified[0] = "Verifizierungsfehler";
			verified[1] = "Bitte melden Sie sich erneut an und starten Sie erneute den Email-Verifizierung Service...";
		}
		
		request.getSession().setAttribute("verified", verified);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/views/index.xhtml");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
