package de.dksc0der.cbb.web.validation;

import java.util.ResourceBundle;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import de.dksc0der.cbb.repository.MemberSecurityService;

@FacesValidator("emailExistValidator")
public class EmailExistValidator implements Validator {

	private ResourceBundle bundle = null;
	
	@EJB
	private MemberSecurityService securityService;
	
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		String email = value.toString();
		
		
		bundle = ResourceBundle.getBundle("languages.language", context.getViewRoot().getLocale());
		
		if(securityService.isMemberExistByEmail(email)){
			FacesMessage msg = new FacesMessage(bundle.getString("validator.email.exist.success"), bundle.getString("validator.email.exist.success.info"));
			msg.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage("input_form:email", msg);
		}else {
			FacesMessage msg = new FacesMessage(bundle.getString("validator.email.exist.error"), bundle.getString("validator.email.exist.error.info"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}

}
