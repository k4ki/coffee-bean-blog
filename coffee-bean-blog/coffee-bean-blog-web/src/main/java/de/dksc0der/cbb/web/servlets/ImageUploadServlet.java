package de.dksc0der.cbb.web.servlets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;

import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.repository.MemberAccountService;


/**
 * Servlet implementation class ImageUploadServlet
 */
@WebServlet("/views/account/imageUploader")
public class ImageUploadServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
     
	@EJB
	private MemberAccountService accountService;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImageUploadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String seperator = System.getProperty("file.separator");
		String image_stream = null; 
	    Member member = (Member)request.getSession().getAttribute("member");
	    image_stream = request.getParameter("image_file");
	    System.out.println(image_stream);
	    if(image_stream == null | member == null){
	    	request.getSession().setAttribute("image_upload_error", "error");
		    response.sendRedirect("account.xhtml");
	    }
	    
	    String[] datas = splitImageStream(image_stream);
	    
	    // to decode the base64 image to byte array
        byte[] imageByteArray = decodeImage(datas[3]);
        
        String path = request.getServletContext().getRealPath(seperator + "resources" + seperator + "img" + seperator + "users" + seperator + member.getUsername());
        String name = getFilename(member.getUsername()) + "." + getExtension(datas[1]);
        String deletename = member.getAvatarpath();
        
        File dir = new File(path);
        File file = new File(dir, name);
        File deleteFile = new File(dir, deletename);
        
        dir.mkdir();
       	
        if(file.createNewFile()){
	        try ( FileOutputStream imageOutFile = new FileOutputStream(file)) {
            imageOutFile.write(imageByteArray);
            imageOutFile.close();
	        }
	        deleteFile.delete();
	        member.setAvatarpath(name);
	        member = accountService.updateAvatar(member);
	        request.getSession().setAttribute("member", member);
	        request.getSession().setAttribute("image_upload_success", "success");
	        response.sendRedirect("account.xhtml");
        }
	}
	
	private String[] splitImageStream(String imageStream){
		String[] split = imageStream.split("(,|;|:)");
		return split;
	}
	
	private byte[] decodeImage(String imageDataString) {
		return Base64.decodeBase64(imageDataString);
	}
	
	private String getExtension(String code){
		String extension = null;        		// to store extension of image
		//checks file type and stores into extension
        if (code.equalsIgnoreCase("image/jpeg")) {
            extension = "jpeg";
        }
        if (code.equalsIgnoreCase("image/png")) {
            extension = "png";
        }
        if (code.equalsIgnoreCase("image/gif")) {
            extension = "gif";
        }
        if (code.equalsIgnoreCase("image/jpg")) {
            extension = "jpg";
        }
        if (code.equalsIgnoreCase("image/bmp")) {
            extension = "bmp";
        }
        if (code.equalsIgnoreCase("image/tiff")) {
            extension = "tiff";
        }
		return extension;
	}
	
	private String getFilename(String username){
		// to get time into milliseconds
        Calendar calendar = Calendar.getInstance();
        long milis = calendar.getTimeInMillis();
        String filename = Long.toString(milis);
        filename = filename + "." + username;
		return filename;
	}
}
