package de.dksc0der.cbb.web.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import de.dksc0der.cbb.model.Category;
import de.dksc0der.cbb.model.Item;
import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.model.Post;
import de.dksc0der.cbb.repository.MemberPostService;

@ViewScoped
@ManagedBean
public class PostController implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private FacesContext facesContext;

	@EJB
	private MemberPostService postService;

	private Post currentPost;

	private boolean authored;

	public Post getCurrentPost() {
		return currentPost;
	}

	public void setCurrentPost(Post currentPost) {
		this.currentPost = currentPost;
	}

	public boolean isAuthored() {
		return authored;
	}

	public void setAuthored(boolean authored) {
		this.authored = authored;
	}
	
	@PostConstruct
	private void initialize() throws IOException{
		currentPost = (Post)facesContext.getExternalContext().getSessionMap().get("editpost");
		
	}

	public void onload() {
		int id = Integer.valueOf(((HttpServletRequest) facesContext.getExternalContext().getRequest()).getParameter("id"));

		currentPost = postService.findPostById(id);

		Member member = (Member) facesContext.getExternalContext().getSessionMap().get("member");

		if (member != null && 
		    member.getUsername().equals(currentPost.getAuthor().getUsername()) && 
			member.getEmail().equals(currentPost.getAuthor().getEmail())) {
			setAuthored(true);
		}
	}
	
	public void delete() throws IOException{
		postService.delete(currentPost);
		
		facesContext.getExternalContext().redirect("/coffee-bean-blog-web/views/index.xhtml");
	}
	
	public void edit() throws IOException{
		facesContext.getExternalContext().getSessionMap().put("editpost", currentPost);
		
		facesContext.getExternalContext().redirect("/coffee-bean-blog-web/views/blog/post-edit.xhtml");
	}
	
	public void savePost(){
		Member member = (Member) facesContext.getExternalContext().getSessionMap().get("member");
		currentPost = postService.create(member, currentPost);
	}
	
	public void saveCategory(Category category){
		
		currentPost = postService.updateCategory(currentPost, category);
	}
	
	public void saveItem(Item item){
		currentPost = postService.updateItem(currentPost, item);
	}
}
