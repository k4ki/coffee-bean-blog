package de.dksc0der.cbb.web.controller;

import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.repository.MemberSecurityService;
import de.dksc0der.cbb.util.AppConstants;
import de.dksc0der.cbb.util.email.EmailTemplateBuilder;
import de.dksc0der.cbb.util.email.types.EmailType;

@ManagedBean
@ViewScoped
public class VerificatorController {
	
	//private static final String NAV_VERIFICATOR = "/views/verification/verificator?faces-redirect=false";
	
	@Resource(name="DefaultMangedExecutorService")
	private ManagedExecutorService executor;
	
	@Inject
	private FacesContext facesContext;
	
	@EJB
	private MemberSecurityService securityService;
	
	private EmailTemplateBuilder emailBuilder; 
	
	private ResourceBundle bundle;
	
	
	private boolean begin;
	private boolean end;
	private boolean emailEdit;
	private boolean reset;
	
	private String email;
	private String confirmEmail;
	
	public boolean isBegin() {
		return begin;
	}
	public void setBegin(boolean begin) {
		this.begin = begin;
	}
	public boolean isEnd() {
		return end;
	}
	public void setEnd(boolean end) {
		this.end = end;
	}
	public boolean isEmailEdit() {
		return emailEdit;
	}
	public void setEmailEdit(boolean emailEdit) {
		this.emailEdit = emailEdit;
	}
	public boolean isReset() {
		return reset;
	}
	public void setReset(boolean reset) {
		this.reset = reset;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getConfirmEmail() {
		return confirmEmail;
	}
	public void setConfirmEmail(String confirmEmail) {
		this.confirmEmail = confirmEmail;
	}
	
	@PostConstruct
	private void initialize(){
		bundle = ResourceBundle.getBundle("languages/language", facesContext.getViewRoot().getLocale());
		emailBuilder = new EmailTemplateBuilder(facesContext.getViewRoot().getLocale());
		setBegin(true);
		setEnd(false);
		setEmailEdit(false);
		setReset(false);
	}
	
	@RolesAllowed({AppConstants.ROLE_ADMIN,AppConstants.ROLE_MEMBER_VERIFIED, AppConstants.ROLE_MEMBER_UNVERIFIED})
	public void sentEmail() throws Exception{
		Member member = (Member)facesContext.getExternalContext().getSessionMap().get("member"); 
		
		if(member == null){
			FacesMessage msg = new FacesMessage(bundle.getString("verificator.controller.error"), bundle.getString("verificator.controller.error.security"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
			
		String token = securityService.beginVerification(member);
		if(token == null || token.equals("")){
			FacesMessage msg = new FacesMessage(bundle.getString("verificator.controller.error"), bundle.getString("verificator.controller.error.server"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
		
		String emailBodyText = emailBuilder.createEmailBodyByTemplate(EmailType.VERIFICATION, token, facesContext.getViewRoot().getLocale());
		String subject = emailBuilder.createEmailSubject(EmailType.VERIFICATION, facesContext.getViewRoot().getLocale());
		
		sendmail(member.getEmail(), "zus3iik4k@gmail.com", subject, emailBodyText);
		
		
		setBegin(false);
		setEnd(true);
		setEmailEdit(false);
		setReset(true);
	}
	
	@RolesAllowed({AppConstants.ROLE_ADMIN,AppConstants.ROLE_MEMBER_VERIFIED, AppConstants.ROLE_MEMBER_UNVERIFIED})
	public void resetVerification(){
		Member member = (Member)facesContext.getExternalContext().getSessionMap().get("member"); 
		member = securityService.resetVerification(member);
		facesContext.getExternalContext().getSessionMap().put("member", member);
		setReset(false);
		
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, bundle.getString("verificator.controller.success"), bundle.getString("verificator.controller.success.verified"));
		facesContext.addMessage(null, msg);
	}
	
	@RolesAllowed({AppConstants.ROLE_ADMIN,AppConstants.ROLE_MEMBER_VERIFIED, AppConstants.ROLE_MEMBER_UNVERIFIED})
	public void resetEmail(){
		setBegin(false);
		setEnd(false);
		setEmailEdit(true);
	}
	
	@RolesAllowed({AppConstants.ROLE_ADMIN,AppConstants.ROLE_MEMBER_VERIFIED, AppConstants.ROLE_MEMBER_UNVERIFIED})
	public void editEmail(){
		Member member = (Member)facesContext.getExternalContext().getSessionMap().get("member");
		member = securityService.emailUpdate(member, email);
		facesContext.getExternalContext().getSessionMap().put("member", member);
		if(member  != null){
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, bundle.getString("verificator.controller.success"), bundle.getString("verificator.controller.success.email.edit"));
			facesContext.addMessage(null, msg);
			
		}else{
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, bundle.getString("verificator.controller.error"), bundle.getString("verificator.controller.error.email.edit"));
			facesContext.addMessage(null, msg);
		}
		setBegin(true);
		setEnd(false);
		setEmailEdit(false);
		setReset(true);
	}
	
	

	@RolesAllowed({AppConstants.ROLE_ADMIN,AppConstants.ROLE_MEMBER_VERIFIED, AppConstants.ROLE_MEMBER_UNVERIFIED})
	public void sendmail(String to, String from, String subject, String body){
		Runnable task = new Runnable() {
			
			@Override
			public void run() {
				Future<Boolean>result = emailBuilder.sender(to, from, subject, body, true);
				
				try {
					result.get();
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
			}
		};
		
		executor.submit(task);
	}
}
