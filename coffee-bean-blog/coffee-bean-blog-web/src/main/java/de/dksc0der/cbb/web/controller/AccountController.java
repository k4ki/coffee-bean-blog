package de.dksc0der.cbb.web.controller;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.repository.MemberAccountService;
import de.dksc0der.cbb.util.EncodeDecodeUtil;

@ViewScoped
@ManagedBean
public class AccountController {

	@Inject
	private FacesContext facesContext;

	@EJB
	private MemberAccountService accountService;

	private Member currentMember;
	private Member newMember;

	private String oldPassword;
	private String newPassword;
	private String confirmPassword;

	@SuppressWarnings("unused")
	private Date birthday;

	private boolean editMemberInfo;
	private boolean editAccountInfo;
	private boolean editPasswordInfo;

	public Member getCurrentMember() {
		return currentMember;
	}

	public void setCurrentMember(Member currentMember) {
		this.currentMember = currentMember;
	}

	public Member getNewMember() {
		return newMember;
	}

	public void setNewMember(Member newMember) {
		this.newMember = newMember;
	}

	public boolean isEditMemberInfo() {
		return editMemberInfo;
	}

	public void setEditMemberInfo(boolean editMemberInfo) {
		this.editMemberInfo = editMemberInfo;
	}

	public boolean isEditAccountInfo() {
		return editAccountInfo;
	}

	public void setEditAccountInfo(boolean editAccountInfo) {
		this.editAccountInfo = editAccountInfo;
	}

	public boolean isEditPasswordInfo() {
		return editPasswordInfo;
	}

	public void setEditPasswordInfo(boolean editPasswordInfo) {
		this.editPasswordInfo = editPasswordInfo;
	}

	public Date getBirthday() {
		return new Date(currentMember.getBirthday().getTime());
	}

	public void setBirthday(Date birthday) {
		currentMember.setBirthday(new java.sql.Date(birthday.getTime()));
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	@PostConstruct
	private void initialize() {
		ExternalContext externalContext = facesContext.getExternalContext();
		currentMember = (Member) externalContext.getSessionMap().get("member");
		// in case went to a specific URL
		if (currentMember == null) {
			HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
			java.security.Principal principal = request.getUserPrincipal();
			if (principal != null) {
				try {
					currentMember = accountService.findByUsername(principal.getName());
				} catch (Exception ignored) {
					// logout whoever and set user to null.
					try {
						((HttpServletRequest) externalContext.getRequest()).logout();
					} catch (ServletException alsoIgnored) {
					}
					externalContext.invalidateSession();
					currentMember = null;
				}
			}
		}
		newMember = currentMember;
	}
	
	public String upload(){
		System.out.println("upload");
		return "/views/account/account?faces-redirect=false";
	}

	public void onload() {
		String error = (String) facesContext.getExternalContext().getSessionMap().remove("image_upload_error");
		String success = (String) facesContext.getExternalContext().getSessionMap().remove("image_upload_success");

		if (error != null) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler", "Bild hochladen schlug fehl...");
			facesContext.addMessage("main_message", msg);
		}
		if (success != null) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Perfekt",
					"Bild wurde erfolgreich hochgeladen und gesetzt...");
			facesContext.addMessage("main_message", msg);
		}
	}

	public void changeEditMemberInfo() {
		editMemberInfo = !editMemberInfo;
	}

	public void changeEditAccountInfo() {
		editAccountInfo = !editAccountInfo;
	}

	public void changeEditPasswordInfo() {
		editPasswordInfo = !editPasswordInfo;
	}

	public String saveMemberInfo() {
		currentMember = accountService.updateMemberInfo(currentMember);
		if (currentMember == null) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler",
					"Benutzer-Informationen konnten nicht gespeichert werden...");
			facesContext.addMessage("main_message", msg);
		} else {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Perfekt",
					"Benutzer-Informationen erfolgreich gesetzt...");
			facesContext.addMessage("main_message", msg);
		}

		return "/views/account/account?faces-redirect=false";
	}

	public String saveAccountInfo() {
		currentMember = accountService.updateSecurity(currentMember);
		if (currentMember == null) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler",
					"Account-Informationen konnten nicht gespeichert werden...");
			facesContext.addMessage("main_message", msg);
		} else {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Perfekt",
					"Account-Informationen erfolgreich gesetzt...");
			facesContext.addMessage("main_message", msg);
		}

		return "/views/account/account?faces-redirect=false";
	}

	public String savePasswordInfo() throws Exception {
		String old = EncodeDecodeUtil.encode(oldPassword);
		if (old.equals(currentMember.getPassword())) {
			
			currentMember.setPassword(newPassword);
			currentMember = accountService.updatePassword(currentMember);
			
			if (currentMember == null) {
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler",
						"Passwort-Informationen konnten nicht gespeichert werden...");
				facesContext.addMessage("main_message", msg);
			} else {
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Perfekt",
						"Passwort-Informationen erfolgreich gesetzt...");
				facesContext.addMessage("main_message", msg);
			}
		} else {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler",
					"Passwort-Informationen konnten nicht gespeichert werden...");
			facesContext.addMessage("main_message", msg);
		}

		return "/views/account/account?faces-redirect=false";
	}
}
