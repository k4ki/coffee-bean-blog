package de.dksc0der.cbb.web.validation;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("lengthLastnameValidator")
public class LengthLastnameValidator implements Validator {

	private ResourceBundle bundle;
	
	private int LENGTH_MAX = 15;
	private int LENGTH_MIN = 2;
	
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		bundle = ResourceBundle.getBundle("languages/language", context.getViewRoot().getLocale());
		String name = value.toString();
		
		if(name.length() < LENGTH_MIN){
			FacesMessage msg = new FacesMessage(bundle.getString("validator.length.lastname.error"), bundle.getString("validator.length.lastname.error.min"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}else if(name.length() > LENGTH_MAX){
			FacesMessage msg = new FacesMessage(bundle.getString("validator.length.lastname.error"), bundle.getString("validator.length.lastname.error.min"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}else {
			FacesMessage msg = new FacesMessage(bundle.getString("validator.length.lastname.success"), bundle.getString("validator.length.lastname.success.info"));
			msg.setSeverity(FacesMessage.SEVERITY_INFO);
			context.addMessage("info_form:lastname-edit", msg);
		}
	}
}
