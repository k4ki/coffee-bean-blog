package de.dksc0der.cbb.web.controller;

import java.sql.Timestamp;

import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;

import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.repository.MemberSecurityService;
import de.dksc0der.cbb.util.AppConstants;
import de.dksc0der.cbb.web.util.Resources;

@ManagedBean
@RequestScoped
public class LoginController {
	
	private static Logger log = Logger.getLogger(LoginController.class.getName());
	
	private static final String NAV_INDEX_REDIRECT = "/views/index?faces-redirect=true";
	
	@Inject
	private FacesContext facesContext;
	
	@EJB
	private MemberSecurityService securityServcie;
	
	private String username;
	private String password;
	
	private Member currentMember;
	
	@PostConstruct
	private void initialize(){
		ExternalContext externalContext = facesContext.getExternalContext();
		currentMember = (Member)externalContext.getSessionMap().get("member");
		
		if(currentMember == null){
			HttpServletRequest request = (HttpServletRequest)externalContext.getRequest();
			java.security.Principal principal = request.getUserPrincipal();
			if(principal != null){
				try {
					currentMember = securityServcie.findByUsername(principal.getName());
				} catch (Exception ignored) {
					try {
						((HttpServletRequest)externalContext.getRequest()).logout();
					} catch (ServletException alsoIngnored) {}
				}
			}
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isLoggedIn(){
		return currentMember != null;
	}
	
	public boolean isAdmin(){
		if(currentMember == null)
			return false;
		return currentMember.isAdmin();
	}
	
	public boolean isMemberUnverified(){
		if(currentMember == null)
			return false;
		return currentMember.isMemberUnverified();
	}
	
	public boolean isMemberVerified(){
		if(currentMember == null)
			return false;
		return currentMember.isMemberVerified();
	}
	
	public String login() throws Exception{
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletRequest request = (HttpServletRequest)externalContext.getRequest();
		
		if(request.getUserPrincipal() == null){
			try {
				request.login(username, password);
				log.info(username + " logged in " + new Timestamp(System.currentTimeMillis()));
				currentMember = securityServcie.findByUsername(username);
				
				currentMember = securityServcie.beginSession(currentMember);
				
				externalContext.getSessionMap().put("member", currentMember);
				
				
				log.info("Logged Member -->> \n" + externalContext.getSessionMap().get("member"));
				return NAV_INDEX_REDIRECT;
			} catch (ServletException ignored) {
				facesContext.addMessage(null, new FacesMessage("Login Failed!!!", ""));
				return NAV_INDEX_REDIRECT;
			}
		}
		return NAV_INDEX_REDIRECT;
	}
	
	@RolesAllowed({AppConstants.ROLE_MEMBER_VERIFIED, AppConstants.ROLE_MEMBER_UNVERIFIED})
	public String logout() throws Exception{
		ExternalContext externalContext = facesContext.getExternalContext();
		HttpServletRequest request = (HttpServletRequest)externalContext.getRequest();
		
		try {
			if(request.getSession().getAttribute("member") != null){
				Member member = ((Member)request.getSession().getAttribute("member"));
				securityServcie.endSession(member);
				
				
				request.logout();
				externalContext.invalidateSession();
				currentMember = null;
				log.info("Logged out Member -->> \n" + externalContext.getSessionMap().get("member"));
			}
		} catch (ServletException e) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Logout failed!!!", Resources.getRootErrorMessage(e)));
			return null;
		}
		return NAV_INDEX_REDIRECT;
	}
	
//	private String findMemberIP(HttpServletRequest request){
//		String ipAddress = request.getHeader("X-FORWARDED-FOR");
//		if(ipAddress != null){
//			ipAddress = ipAddress.replaceFirst(",.*", "");
//		}else{
//			ipAddress = request.getRemoteAddr();
//		}
//		return ipAddress;
//	}
}
