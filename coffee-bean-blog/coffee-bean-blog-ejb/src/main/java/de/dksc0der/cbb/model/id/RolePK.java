package de.dksc0der.cbb.model.id;

import java.io.Serializable;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
public class RolePK implements Serializable{

	private static final long serialVersionUID = 1L;

	private String rolename;

	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	
	@Override
	public int hashCode() {
		return (int)rolename.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean equal = false;
		if(obj instanceof RolePK){
			RolePK rolePK = (RolePK) obj;
			if(rolePK.getRolename().equals(this.getRolename())){
				equal = true;
			}
		}
		return equal;
	}
}
