package de.dksc0der.cbb.repository;

import javax.ejb.Remote;

import de.dksc0der.cbb.model.Member;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Remote
public interface MemberSecurityService {
	Member beginSession(Member member);
	Member endSession(Member member);
	
	String beginVerification(Member member);
	Member resetVerification(Member member);
	Member endVerification(String token);
	
	String passwordReset(String email);
	Member passwordUpdate(String checksum, String password);
	
	Member emailUpdate(Member member, String email);
	
	Member findByUsername(String username);
	Member findByEmail(String email);
	Member findByToken(String username, String email, Long createdChecksum, String checksum);
	boolean isMemberExistByUsername(String username);
	boolean isMemberExistByEmail(String email);
	long countAllMember();
}
