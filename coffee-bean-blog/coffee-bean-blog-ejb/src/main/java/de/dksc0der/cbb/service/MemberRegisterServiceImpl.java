package de.dksc0der.cbb.service;

import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.jboss.ejb3.annotation.SecurityDomain;
import org.jboss.logging.Logger;

import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.model.Role;
import de.dksc0der.cbb.repository.MemberRegisterService;
import de.dksc0der.cbb.repository.MemberRoleService;
import de.dksc0der.cbb.util.EncodeDecodeUtil;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Stateless
@SecurityDomain("coffee-bean-blog")
public class MemberRegisterServiceImpl implements MemberRegisterService {

	private static Logger log = Logger.getLogger(MemberRegisterServiceImpl.class.getName());
	
	@Inject
	private EntityManager em;
	
	@EJB
	private MemberRoleService roleService;
	
	@Override
	@PermitAll
	public Member saveAndEncode(Member member){
		log.info("CLASS::MemberRegisterServiceImpl --> saveAndEncode::BEGIN");
		TypedQuery<Long> q = em.createNamedQuery(Member.COUNT_BY_USERNAME, Long.class).setParameter("username", member.getUsername());
        if ( q.getSingleResult().longValue() > 0L ) {
            // show error condition
            return null;
        }
        // Encode password
        try {
			member.setPassword(EncodeDecodeUtil.encode(member.getPassword()));
        } catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Error by encode password...");
		}
        // Add role "MEMVER_UNVERIFIED" to member.
        Role role = roleService.getMemberUnVerifiedRole();
        Set<Role> roles = new CopyOnWriteArraySet<Role>();
        roles.add(em.merge(role));
        member.setRoles(roles);
        // Set created time
        member.setCreateddate(new Timestamp(System.currentTimeMillis()));
        member.setVerified(false);
        // Persist user.
        em.persist(member);
        em.flush();
        
		log.info("CLASS::MemberRegisterServiceImpl --> saveAndEncode::END");
		return em.merge(member);
	}

	@Override
	@PermitAll
	public long countAllMember() {
		log.info("CLASS::MemberRegisterServiceImpl --> countAllMember::BEGIN");
		long count = em.createNamedQuery(Member.COUNT_ALL, Long.class).getSingleResult();
		//log.info("CLASS::MemberRegisterServiceImpl --> countAllMember -->> RESULT(COUNT) =  \n" + count);
		log.info("CLASS::MemberRegisterServiceImpl --> countAllMember::END");
		return count;
	}
}
