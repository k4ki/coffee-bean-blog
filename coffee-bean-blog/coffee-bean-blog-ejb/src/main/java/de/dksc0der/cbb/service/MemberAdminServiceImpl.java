package de.dksc0der.cbb.service;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.jboss.ejb3.annotation.SecurityDomain;
import org.jboss.logging.Logger;

import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.model.Role;
import de.dksc0der.cbb.model.id.MemberPK;
import de.dksc0der.cbb.repository.MemberAdminService;
import de.dksc0der.cbb.repository.MemberRoleService;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Stateless
@SecurityDomain("coffee-bean-blog")
public class MemberAdminServiceImpl implements MemberAdminService {

	private static Logger log = Logger.getLogger(MemberAdminServiceImpl.class.getName());

	@Inject
	private EntityManager em;
	
	@EJB
	private MemberRoleService roleService;
	
	@Override
	@RolesAllowed("ADMIN")
	public Member promote(Member member, Role role) {
		log.info("CLASS::MemberAdminServiceImpl --> create::BEGIN");
		member = findByUsername(member.getUsername());
		Set<Role>roles = roleService.findAll();
		Iterator<Role>itrRole = roles.iterator();
		while (itrRole.hasNext()) {
			Role findRole = (Role) itrRole.next();
			if(findRole.getRolename().equals(role.getRolename())){
				member.getRoles().add(findRole);
			}
		}
		member = em.merge(member);
		//log.info("CLASS::MemberAdminServiceImpl --> promote -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberAdminServiceImpl --> promote::END");
		return member;
	}

	@Override
	@RolesAllowed("ADMIN")
	public Member demote(Member member, Role role) {
		log.info("CLASS::MemberAdminServiceImpl --> create::BEGIN");
		member = findByUsername(member.getUsername());
		Set<Role>roles = member.getRoles();
		Iterator<Role>itrRole = roles.iterator();
		while (itrRole.hasNext()) {
			Role findRole = (Role) itrRole.next();
			if(findRole.getRolename().equals(role.getRolename())){
				itrRole.remove();
			}
		}
		member = em.merge(member);
		//log.info("CLASS::MemberAdminServiceImpl --> demote -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberAdminServiceImpl --> demote::END");
		return member;
	}

	@Override
	@RolesAllowed("ADMIN")
	public Member findByUsername(String username) {
		log.info("CLASS::MemberAdminServiceImpl --> findByUsername::BEGIN");
		MemberPK memberPK = new MemberPK();
		memberPK.setUsername(username);
		
		Member member = em.find(Member.class, memberPK);
		//log.info("CLASS::MemberAdminServiceImpl --> findByUsername -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberAdminServiceImpl --> findByUsername::END");
		return member;
	}

	@Override
	@RolesAllowed("ADMIN")
	public Set<Member> findAll() {
		log.info("CLASS::MemberAdminServiceImpl --> findAll::BEGIN");
		Set<Member>members = new CopyOnWriteArraySet<Member>(em.createNamedQuery(Member.FIND_ALL, Member.class).getResultList());
		//log.info("CLASS::MemberAdminServiceImpl --> findAll -->> RESULT(MEMBERS) =  \n" + members);
		log.info("CLASS::MemberAdminServiceImpl --> findAll::END");
		return members;
	}

}
