package de.dksc0der.cbb.model;

import static de.dksc0der.cbb.util.AppConstants.NEW_LINE;
import static de.dksc0der.cbb.util.AppConstants.FILL_LINE;
import static de.dksc0der.cbb.util.AppConstants.TEMPLATE_ARROWS;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.dksc0der.cbb.model.id.RolePK;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Entity
@IdClass(RolePK.class)
@Table(name="role")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="role")
@NamedQueries({
	@NamedQuery(name=Role.FIND_ALL, query="SELECT r FROM Role r")
})
public class Role implements Serializable{

	private static final long serialVersionUID = 1L;

	public final static String FIND_ALL = "role.findAll";
	
	@Id
	@Column(name="rolename")
	@XmlElement(name="rolename")
	private String rolename;
	
	@Version
	@Column(name="updateddate")
	@XmlElement(name="updateddate")
	private Timestamp updateddate;

	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public Timestamp getUpdateddate() {
		return updateddate;
	}

	public void setUpdateddate(Timestamp updateddate) {
		this.updateddate = updateddate;
	}
	
	public Role() {}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(FILL_LINE).append(NEW_LINE)
		.append("ROLENAME").append(TEMPLATE_ARROWS).append(this.getRolename()).append(NEW_LINE)
		.append("UPDATED").append(TEMPLATE_ARROWS).append(this.getUpdateddate()).append(NEW_LINE);
		return buffer.toString();
	}
}
