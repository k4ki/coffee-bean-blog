package de.dksc0der.cbb.service;

import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.Set;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.jboss.ejb3.annotation.SecurityDomain;
import org.jboss.logging.Logger;

import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.model.Role;
import de.dksc0der.cbb.model.id.MemberPK;
import de.dksc0der.cbb.repository.MemberRoleService;
import de.dksc0der.cbb.repository.MemberSecurityService;
import de.dksc0der.cbb.util.AppConstants;
import de.dksc0der.cbb.util.EncodeDecodeUtil;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Stateless
@SecurityDomain("coffee-bean-blog")
public class MemberSecurityServiceImpl implements MemberSecurityService {

	private static Logger log = Logger.getLogger(MemberSecurityServiceImpl.class.getName());
	
	@Inject
	private EntityManager em;
	
	@EJB
	private MemberRoleService roleService;
	
	@Override
	@RolesAllowed({AppConstants.ROLE_MEMBER_UNVERIFIED, AppConstants.ROLE_MEMBER_VERIFIED})
	public Member beginSession(Member member) {
		log.info("CLASS::MemberSecurityServiceImpl --> beginSession::BEGIN");
		member = findByUsername(member.getUsername());
		member.setLastLogin(new Timestamp(System.currentTimeMillis()));
		member.setActive(true);
		
		member = em.merge(member);
		//log.info("CLASS::MemberSecurityServiceImpl --> beginSession -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberSecurityServiceImpl --> beginSession::END");
		return member;
	}

	@Override
	@RolesAllowed({AppConstants.ROLE_MEMBER_UNVERIFIED, AppConstants.ROLE_MEMBER_VERIFIED})
	public Member endSession(Member member) {
		log.info("CLASS::MemberSecurityServiceImpl --> endSession::BEGIN");
		member = findByUsername(member.getUsername());
		member.setActive(false);
		member.setChecksum("");
		member.setLastLogout(new Timestamp(System.currentTimeMillis()));
		member.setCreatedChecksum(0);
		member = em.merge(member);
		//log.info("CLASS::MemberSecurityServiceImpl --> endSession -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberSecurityServiceImpl --> endSession::END");
		return member;
	}

	@Override
	@RolesAllowed({AppConstants.ROLE_MEMBER_UNVERIFIED})
	public String beginVerification(Member member)  {
		log.info("CLASS::MemberSecurityServiceImpl --> beginVerification::BEGIN");
		member = findByUsername(member.getUsername());
		member.setCreatedChecksum(System.currentTimeMillis());
		
		String checksum = null;    
		try {
			checksum = EncodeDecodeUtil.encode(member.getUsername() + "#" + member.getEmail() + "#" +member.getCreatedChecksum());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		member.setChecksum(checksum);
		member = em.merge(member);
		
		String token = EncodeDecodeUtil.encodeToLinkParameter(member.getUsername(), member.getEmail(), member.getCreatedChecksum(), member.getChecksum());
		
		//log.info("CLASS::MemberSecurityServiceImpl --> beginVerification -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberSecurityServiceImpl --> beginVerification::END");
		return token;
	}
	
	@Override
	@RolesAllowed({AppConstants.ROLE_MEMBER_UNVERIFIED})
	public Member resetVerification(Member member) {
		log.info("CLASS::MemberSecurityServiceImpl --> resetVerification::BEGIN");
		member = findByUsername(member.getUsername());
		member.setChecksum("");
		member.setCreatedChecksum(0);
		log.info("CLASS::MemberSecurityServiceImpl --> resetVerification::END");
		
		return em.merge(member);
	}

	@Override
	@RolesAllowed({AppConstants.ROLE_MEMBER_UNVERIFIED})
	public Member endVerification(String token) {
		log.info("CLASS::MemberSecurityServiceImpl --> endVerification::BEGIN");
		String[] clean 				= EncodeDecodeUtil.decodeToValues(token);
		String username 			= clean[0];
		String email 				= clean[1];
		Long createdChecksum 		= Long.parseLong(clean[2]);
		String checksum 			= clean[3];
		
		Member member = findByToken(username, email, createdChecksum, checksum);
		
		if(member == null)
			throw new RuntimeException("Token is not equals to member..."); 
		
		member.setVerified(true);
		member.setChecksum("");
		member.setCreatedChecksum(0);
		
		Set<Role>roles = member.getRoles();
		Iterator<Role>itr = roles.iterator();
		while (itr.hasNext()) {
			Role role = (Role) itr.next();
			if(role.getRolename().equals(AppConstants.ROLE_MEMBER_UNVERIFIED)){
				itr.remove();
				break;
			}
		}
		
		Role role = roleService.getMemberVerifiedRole();
		roles.add(role);
		
		member = em.merge(member);
		//log.info("CLASS::MemberSecurityServiceImpl --> endVerification -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberSecurityServiceImpl --> endVerification::END");
		return member;
	}

	@Override
	@PermitAll
	public String passwordReset(String email) {
		log.info("CLASS::MemberSecurityServiceImpl --> passwordReset::BEGIN");
		Member member = findByEmail(email);
		member.setCreatedChecksum(System.currentTimeMillis());
		
		String checksum = null;    
		try {
			checksum = EncodeDecodeUtil.encode(member.getUsername() + "#" + member.getEmail() + "#" +member.getCreatedChecksum());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		member.setChecksum(checksum);    
		member = em.merge(member);
		
		String token = EncodeDecodeUtil.encodeToLinkParameter(member.getUsername(), member.getEmail(), member.getCreatedChecksum(), member.getChecksum());

		//log.info("CLASS::MemberSecurityServiceImpl --> passwordReset -->> RESULT(TOKEN) =  \n" + token);
		log.info("CLASS::MemberSecurityServiceImpl --> passwordReset::END");
		return token;
	}

	@Override
	@PermitAll
	public Member passwordUpdate(String token, String password) {
		log.info("CLASS::MemberSecurityServiceImpl --> passwordUpdate::BEGIN");
		String[] clean 				= EncodeDecodeUtil.decodeToValues(token);
		String username 			= clean[0];
		String email 				= clean[1];
		Long createdChecksum 		= Long.parseLong(clean[2]);
		String checksum 			= clean[3];
		
		Member member = findByToken(username, email, createdChecksum, checksum);
		
		if(member == null)
			throw new RuntimeException("Token is not equals to member..."); 
		
		member.setVerified(true);
		member.setChecksum("");
		member.setCreatedChecksum(0);
		try {
			member.setPassword(EncodeDecodeUtil.encode(password));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		member = em.merge(member);

		//log.info("CLASS::MemberSecurityServiceImpl --> passwordUpdate -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberSecurityServiceImpl --> passwordUpdate::END");
		return member;
	}

	@Override
	@RolesAllowed({AppConstants.ROLE_MEMBER_UNVERIFIED})
	public Member emailUpdate(Member member, String email) {
		log.info("CLASS::MemberSecurityServiceImpl --> emailUpdate::BEGIN");
		member = findByUsername(member.getUsername());
		member.setEmail(email);
		member = em.merge(member);

		//log.info("CLASS::MemberSecurityServiceImpl --> emailUpdate -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberSecurityServiceImpl --> emailUpdate::END");
		return member;
	}
	
	@Override
	@PermitAll
	public Member findByToken(String username, 
							  String email, 
							  Long createdChecksum, 
							  String checksum){
		log.info("CLASS::MemberSecurityServiceImpl --> findByToken::BEGIN");
		Member member = em.createNamedQuery(Member.FIND_BY_TOKEN, Member.class)
				 .setParameter("username", username)
				 .setParameter("email", email)
				 .setParameter("createdChecksum", createdChecksum)
				 .setParameter("checksum", checksum)
				 .getSingleResult();

		//log.info("CLASS::MemberSecurityServiceImpl --> findByToken -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberSecurityServiceImpl --> findByToken::END");
		return member;
	}

	@Override
	@PermitAll
	public Member findByUsername(String username) {
		log.info("CLASS::MemberSecurityServiceImpl --> findByUsername::BEGIN");
		MemberPK memberPK = new MemberPK();
		memberPK.setUsername(username);
		Member member =  em.find(Member.class, memberPK);
		//log.info("CLASS::MemberSecurityServiceImpl --> findByUsername -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberSecurityServiceImpl --> findByUsername::END");
		return member;
	}

	@Override
	@PermitAll
	public Member findByEmail(String email) {
		log.info("CLASS::MemberSecurityServiceImpl --> findByEmail::BEGIN");
		Member member =  em.createNamedQuery(Member.FIND_BY_EMAIL, Member.class)
				 .setParameter("email", email)
				 .getSingleResult();
		//log.info("CLASS::MemberSecurityServiceImpl --> findByEmail -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberSecurityServiceImpl --> findByEmail::END");
		return member;
	}

	@Override
	@PermitAll
	public boolean isMemberExistByUsername(String username) {
		log.info("CLASS::MemberSecurityServiceImpl --> isMemberExistByUsername::BEGIN");
		TypedQuery<Long> q = em.createNamedQuery(Member.COUNT_BY_USERNAME, Long.class).setParameter("username", username);
        if ( q.getSingleResult().longValue() > 0L ) {
        	log.info("CLASS::MemberSecurityServiceImpl --> isMemberExistByUsername::END");
            // show error condition
            return true;
        }
        log.info("CLASS::MemberSecurityServiceImpl --> isMemberExistByUsername::END");
		return false;
	}

	@Override
	@PermitAll
	public boolean isMemberExistByEmail(String email) {
		log.info("CLASS::MemberSecurityServiceImpl --> isMemberExistByEmail::BEGIN");
		TypedQuery<Long> q = em.createNamedQuery(Member.COUNT_BY_EMAIL, Long.class).setParameter("email", email);
        if ( q.getSingleResult().longValue() > 0L ) {
        	log.info("CLASS::MemberSecurityServiceImpl --> isMemberExistByEmail::END");
            // show error condition
            return true;
        }
        log.info("CLASS::MemberSecurityServiceImpl --> isMemberExistByEmail::END");
		return false;
	}

	@Override
	@PermitAll
	public long countAllMember() {
		log.info("CLASS::MemberSecurityServiceImpl --> findAll::BEGIN");
		long count = em.createNamedQuery(Member.COUNT_ALL, Long.class).getSingleResult();
		//log.info("CLASS::MemberSecurityServiceImpl --> countAllMember -->> RESULT(COUNT) =  \n" + count);
		log.info("CLASS::MemberSecurityServiceImpl --> countAllMember::END");
		return count;
	}

}
