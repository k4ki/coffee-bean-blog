package de.dksc0der.cbb.repository;

import javax.ejb.Remote;

import de.dksc0der.cbb.model.Category;
import de.dksc0der.cbb.model.Member;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Remote
public interface MemberAccountService {
	Member updateSecurity(Member member);
	Member updateMemberInfo(Member member);
	Member updateMemberCategory(Member member, Category category);
	Member deleteMemberCategory(Member member, Category category);
	Member updatePassword(Member member);
	Member updateAvatar(Member member);
	Member findByUsername(String username);
}
