package de.dksc0der.cbb.service;

import java.sql.Timestamp;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.jboss.ejb3.annotation.SecurityDomain;
import org.jboss.logging.Logger;

import de.dksc0der.cbb.model.Comment;
import de.dksc0der.cbb.model.Item;
import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.model.Post;
import de.dksc0der.cbb.model.id.MemberPK;
import de.dksc0der.cbb.repository.ItemService;
import de.dksc0der.cbb.repository.MemberCommentsService;
import de.dksc0der.cbb.util.AppConstants;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Stateless
@SecurityDomain("coffee-bean-blog")
public class MemberCommentServiceImpl implements MemberCommentsService {

	private static Logger log = Logger.getLogger(MemberCommentServiceImpl.class.getName());

	@Inject
	private EntityManager em;
	
	@EJB
	private ItemService itemService;
	
	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Comment create(Member member, Post post, Comment comment) {
		log.info("CLASS::MemberCommentServiceImpl --> create::BEGIN");
		post = findPostByID(post.getId());
		member = findMemberByUsername(member.getUsername());

		comment.setPost(post);
		comment.setAuthor(member);
		comment.setCreateddate(new Timestamp(System.currentTimeMillis()));
		comment = em.merge(comment);
		em.flush();
		//log.info("CLASS::MemberCommentServiceImpl --> create -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberCommentServiceImpl --> create::END");
		return comment;
	}


	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public void delete(Comment comment) {
		log.info("CLASS::MemberCommentServiceImpl --> delete::BEGIN");
		comment = findCommentById(comment.getId());
		em.remove(comment);
		//log.info("CLASS::MemberCommentServiceImpl --> delete -->> RESULT(COMMENT) =  \n" + comment);
		log.info("CLASS::MemberCommentServiceImpl --> delete::END");
	}

	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Comment updateItem(Comment comment, Item item) {
		log.info("CLASS::MemberCommentServiceImpl --> updateItem::BEGIN");
		item = itemService.create(item);
		comment = findCommentById(comment.getId());
		comment.getItems().add(item);
		comment = em.merge(comment);
		//log.info("CLASS::MemberCommentServiceImpl --> updateItem -->> RESULT(COMMENT) =  \n" + comment);
		log.info("CLASS::MemberCommentServiceImpl --> updateItem::END");
		return comment;
	}


	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Comment deleteItem(Comment comment, Item item) {
		log.info("CLASS::MemberCommentServiceImpl --> deleteItem::BEGIN");
		comment = findCommentById(comment.getId());
		Iterator<Item>itrItems = comment.getItems().iterator();
		while (itrItems.hasNext()) {
			Item deleteItem = (Item) itrItems.next();
			if(deleteItem.getId() == item.getId())
				itrItems.remove();
			
		}
		comment = em.merge(comment);
		//log.info("CLASS::MemberCommentServiceImpl --> deleteItem -->> RESULT(COMMENT) =  \n" + comment);
		log.info("CLASS::MemberCommentServiceImpl --> deleteItem::END");
		return comment;
	}


	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Comment findCommentById(long id) {
		log.info("CLASS::MemberCommentServiceImpl --> findCommentById::BEGIN");
		Comment comment = em.find(Comment.class, id);
		//log.info("CLASS::MemberCommentServiceImpl --> findCommentById -->> RESULT(MEMBER) =  \n" + comment);
		log.info("CLASS::MemberCommentServiceImpl --> findCommentById::END");
		return comment;
	}

	@Override
	@PermitAll
	public Set<Comment> findCommentsByUsername(String username) {
		log.info("CLASS::MemberCommentServiceImpl --> findCommentsByUsername::BEGIN");
		Set<Comment>comments = new CopyOnWriteArraySet<Comment>(em.createNamedQuery(Comment.FIND_BY_USERNAME, Comment.class)
				   												  .setParameter("username", username)
				   												  .getResultList());
		//log.info("CLASS::MemberCommentServiceImpl --> findCommentsByUsername -->> RESULT(MEMBER) =  \n" + comments);
		log.info("CLASS::MemberCommentServiceImpl --> findCommentsByUsername::END");
		return comments;
	}

	@Override
	@PermitAll
	public Set<Comment> findCommentByPost(Post post) {
		log.info("CLASS::MemberCommentServiceImpl --> findCommentByPost::BEGIN");
		Set<Comment>comments = new CopyOnWriteArraySet<Comment>(em.createNamedQuery(Comment.FIND_BY_POST, Comment.class)
															      .setParameter("postid", post.getId())
															      .getResultList());
		//log.info("CLASS::MemberCommentServiceImpl --> findCommentByPost -->> RESULT(MEMBER) =  \n" + comments);
		log.info("CLASS::MemberCommentServiceImpl --> findCommentByPost::END");
		return comments;
	}

	@Override
	@PermitAll
	public Post findPostByID(long id) {
		log.info("CLASS::MemberCommentServiceImpl --> findPostByID::BEGIN");
		Post post = em.find(Post.class, id);
		//log.info("CLASS::MemberCommentServiceImpl --> findPostByID -->> RESULT(MEMBER) =  \n" + post);
		log.info("CLASS::MemberCommentServiceImpl --> findPostByID::END");
		return post;
	}

	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Member findMemberByUsername(String username) {
		log.info("CLASS::MemberCommentServiceImpl --> findMemberByUsername::BEGIN");
		MemberPK memberPK = new MemberPK();
		memberPK.setUsername(username);
		Member member = em.find(Member.class, memberPK);
		//log.info("CLASS::MemberCommentServiceImpl --> findMemberByUsername -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberCommentServiceImpl --> findMemberByUsername::END");
		return member;
	}

}
