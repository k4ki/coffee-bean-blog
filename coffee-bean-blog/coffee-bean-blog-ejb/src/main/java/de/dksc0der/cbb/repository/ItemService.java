package de.dksc0der.cbb.repository;

import javax.ejb.Remote;

import de.dksc0der.cbb.model.Item;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Remote
public interface ItemService {

	Item create(Item item);
	Item update(Item item);
	void delete(Item item);
	Item findByID(long id);
}
