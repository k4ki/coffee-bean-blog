package de.dksc0der.cbb.service;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.jboss.ejb3.annotation.SecurityDomain;
import org.jboss.logging.Logger;

import de.dksc0der.cbb.model.Category;
import de.dksc0der.cbb.model.id.CategoryPK;
import de.dksc0der.cbb.repository.CategoryService;
import de.dksc0der.cbb.util.AppConstants;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Stateless
@SecurityDomain("coffee-bean-blog")
public class CategoryServiceImpl implements CategoryService {

	private static Logger log = Logger.getLogger(CategoryServiceImpl.class.getName());
	
	@Inject
	private EntityManager em;
	
	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Category create(Category category) {
		log.info("CLASS::CategoryServiceImpl --> create::BEGIN");
		Set<Category>allCategories = findAll();
		
		Iterator<Category>itrAllCategories = allCategories.iterator();
		while (itrAllCategories.hasNext()) {
			Category allCategory = (Category) itrAllCategories.next();
			if(allCategory.getCategoryname().equals(category.getCategoryname())){
				allCategory = findByCategoryName(allCategory.getCategoryname());
				return allCategory;
			}
		}
		category = em.merge(category);
		em.flush();
		//log.info("CLASS::CategoryServiceImpl --> create -->> RESULT(CATEGORY) =  \n" + category);
		log.info("CLASS::CategoryServiceImpl --> create::END");
		return category;
	}

	@Override
	@PermitAll
	public Category findByCategoryName(String categoryName) {
		log.info("CLASS::CategoryServiceImpl --> findByCategoryName::BEGIN");
		CategoryPK categoryPK = new CategoryPK();
		categoryPK.setCategoryname(categoryName);
		Category category = em.find(Category.class, categoryPK);
		//log.info("CLASS::CategoryServiceImpl --> findByCategoryName -->> RESULT(CATEGORY) =  \n" + category);
		log.info("CLASS::CategoryServiceImpl --> findByCategoryName::END");
		return category;
	}

	@Override
	@PermitAll
	public Set<Category> findAll() {
		log.info("CLASS::CategoryServiceImpl --> findAll::BEGIN");
		Set<Category> categories = new CopyOnWriteArraySet<Category>(em.createNamedQuery(Category.FIND_ALL, Category.class).getResultList());
		//log.info("CLASS::CategoryServiceImpl --> findAll -->> RESULT(CATEGORIES) =  \n" + categories);
		log.info("CLASS::CategoryServiceImpl --> findAll::END");
		return categories;
	}

}
