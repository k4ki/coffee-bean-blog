package de.dksc0der.cbb.service;

import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.jboss.ejb3.annotation.SecurityDomain;
import org.jboss.logging.Logger;

import de.dksc0der.cbb.model.Category;
import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.model.id.MemberPK;
import de.dksc0der.cbb.repository.CategoryService;
import de.dksc0der.cbb.repository.MemberAccountService;
import de.dksc0der.cbb.util.AppConstants;
import de.dksc0der.cbb.util.EncodeDecodeUtil;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Stateless
@SecurityDomain("coffee-bean-blog")
public class MemberAccountServiceImpl implements MemberAccountService{

	private static Logger log = Logger.getLogger(MemberAccountServiceImpl.class.getName());
	
	@Inject
	private EntityManager em;
	
	@EJB
	private CategoryService categoryService;
	
	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Member updateSecurity(Member member) {
		log.info("CLASS::MemberAccountServiceImpl --> updateSecurity::BEGIN");
		Member findMember = findByUsername(member.getUsername());
		findMember.setEmail(member.getEmail());
		findMember = em.merge(findMember);
		//log.info("CLASS::MemberAccountServiceImpl --> updateSecurity -->> RESULT(MEMBER) =  \n" + findMember);
		log.info("CLASS::MemberAccountServiceImpl --> updateSecurity::END");
		return findMember;
	}

	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Member updateMemberInfo(Member member) {
		log.info("CLASS::MemberAccountServiceImpl --> updateMemberInfo::BEGIN");
		Member findMember = findByUsername(member.getUsername());
		findMember.setFirstname(member.getFirstname());
		findMember.setLastname(member.getLastname());
		findMember.setBirthday(member.getBirthday());
		findMember.setDiscription(member.getDiscription());
		findMember = em.merge(findMember);
		//log.info("CLASS::MemberAccountServiceImpl --> updateMemberInfo -->> RESULT(MEMBER) =  \n" + findMember);
		log.info("CLASS::MemberAccountServiceImpl --> updateMemberInfo::END");
		return findMember;
	}


	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Member updateMemberCategory(Member member, Category category) {
		log.info("CLASS::MemberAccountServiceImpl --> updateMemberCategory");
		category = categoryService.create(category);
		//log.info("CLASS::MemberAccountServiceImpl --> updateMemberCategory -->> RESULT(category) =  " + category);
		member = findByUsername(member.getUsername());
		member.getCategories().add(em.merge(category));
		member = em.merge(member);
		//log.info("CLASS::MemberAccountServiceImpl --> updateMemberCategory -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberAccountServiceImpl --> updateMemberCategory::END");
		return member;
	}

	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Member deleteMemberCategory(Member member, Category category) {
		//log.info("CLASS::MemberAccountServiceImpl --> deleteMemberCategory");
		member = findByUsername(member.getUsername());
		Iterator<Category>itrCategories = member.getCategories().iterator();
		while (itrCategories.hasNext()) {
			Category deleted = (Category) itrCategories.next();
			if(deleted.getCategoryname().equals(category.getCategoryname())){
				itrCategories.remove();
			}
			
		}
		member = em.merge(member);
		//log.info("CLASS::MemberAccountServiceImpl --> deleteMemberCategory -->> RESULT(category) =  " + category);
		//log.info("CLASS::MemberAccountServiceImpl --> deleteMemberCategory -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberAccountServiceImpl --> deleteMemberCategory::END");
		return member;
	}

	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Member updatePassword(Member member) {
		log.info("CLASS::MemberAccountServiceImpl --> updatePassword::BEGIN");
		Member findMember = findByUsername(member.getUsername());
		try {
			findMember.setPassword(EncodeDecodeUtil.encode(member.getPassword()));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		findMember = em.merge(findMember);
		//log.info("CLASS::MemberAccountServiceImpl --> updatePassword -->> RESULT(MEMBER) =  \n" + findMember);
		log.info("CLASS::MemberAccountServiceImpl --> updatePassword::END");
		return findMember;
	}

	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Member updateAvatar(Member member) {
		log.info("CLASS::MemberAccountServiceImpl --> updateAvatar::BEGIN");
		Member findMember = findByUsername(member.getUsername());
		findMember.setAvatarpath(member.getAvatarpath());
		findMember = em.merge(findMember);
		//log.info("CLASS::MemberAccountServiceImpl --> updateAvatar -->> RESULT(MEMBER) =  \n" + findMember);
		log.info("CLASS::MemberAccountServiceImpl --> updateAvatar::END");
		return findMember;
	}

	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Member findByUsername(String username) {
		log.info("CLASS::MemberAccountServiceImpl --> findByUsername::BEGIN");
		MemberPK memberPK = new MemberPK();
		memberPK.setUsername(username);
		Member member = em.find(Member.class, memberPK);
		//log.info("CLASS::MemberAccountServiceImpl --> findByUsername -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberAccountServiceImpl --> findByUsername::END");
		return member;
	}

}
