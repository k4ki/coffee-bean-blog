package de.dksc0der.cbb.repository;

import java.util.Set;

import javax.ejb.Remote;

import de.dksc0der.cbb.model.Role;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Remote
public interface MemberRoleService {
	Set<Role>findAll();
	Role getAdminRole();
	Role getMemberVerifiedRole();
	Role getMemberUnVerifiedRole();
}
