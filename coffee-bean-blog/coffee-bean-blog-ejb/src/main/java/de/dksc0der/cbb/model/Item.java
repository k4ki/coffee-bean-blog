package de.dksc0der.cbb.model;

import static de.dksc0der.cbb.util.AppConstants.FILL_LINE;
import static de.dksc0der.cbb.util.AppConstants.NEW_LINE;
import static de.dksc0der.cbb.util.AppConstants.TEMPLATE_ARROWS;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Entity
@Table(name="item")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="item")
public class Item implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	@XmlElement(name="id")
	private long id;
	
	@Lob
	@Column(name="content")
	@XmlElement(name="content")
	private String content;
	
	@Version
	@Column(name="updateddate")
	@XmlElement(name="updateddate")
	private Timestamp updateddate;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="post_id", referencedColumnName="id", nullable=true)
	@XmlTransient
	private Post post;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="comment_id", referencedColumnName="id", nullable=true)
	@XmlTransient
	private Comment comment;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	public Timestamp getUpdateddate() {
		return updateddate;
	}

	public void setUpdateddate(Timestamp updateddate) {
		this.updateddate = updateddate;
	}
	public Item() {}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(FILL_LINE).append(NEW_LINE)
		.append("ID").append(TEMPLATE_ARROWS).append(this.getId()).append(NEW_LINE)
		.append("CONTENT").append(TEMPLATE_ARROWS).append(this.getContent()).append(NEW_LINE)
		.append("UPDATED").append(TEMPLATE_ARROWS).append(this.getUpdateddate()).append(NEW_LINE);
		return buffer.toString();
	}
}
