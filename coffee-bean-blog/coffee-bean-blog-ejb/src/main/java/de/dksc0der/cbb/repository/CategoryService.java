package de.dksc0der.cbb.repository;

import java.util.Set;

import javax.ejb.Remote;

import de.dksc0der.cbb.model.Category;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Remote
public interface CategoryService {
	Category create(Category category);
	Category findByCategoryName(String categoryName);
	
	Set<Category> findAll();
}
