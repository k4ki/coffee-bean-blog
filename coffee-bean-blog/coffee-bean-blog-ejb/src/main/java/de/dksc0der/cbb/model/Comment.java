package de.dksc0der.cbb.model;

import static de.dksc0der.cbb.util.AppConstants.FILL_LINE;
import static de.dksc0der.cbb.util.AppConstants.NEW_LINE;
import static de.dksc0der.cbb.util.AppConstants.TEMPLATE_ARROWS;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Entity
@Table(name="comment")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="comment")
@NamedQueries({
	@NamedQuery(name=Comment.FIND_BY_USERNAME, query="SELECT c FROM Comment c WHERE c.author.username=:username"),
	@NamedQuery(name=Comment.FIND_BY_POST, query="SELECT c FROM Comment c WHERE c.post.id=:postid")
})
public class Comment implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public final static String FIND_BY_USERNAME = "comment.findByUsername";
	public final static String FIND_BY_POST 	= "comment.findByPost";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	@XmlElement(name="id")
	private long id;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="post_id", referencedColumnName="id")
	@XmlTransient
	private Post post;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="author", referencedColumnName="username")
	@XmlElement(name="author")
	private Member author;
	
	@Column(name="title")
	@XmlElement(name="title")
	private String title;
	
	@Lob
	@Column(name="discription")
	@XmlElement(name="discription")
	private String discription;
	
	@OneToMany(mappedBy = "comment", fetch = FetchType.EAGER, orphanRemoval = true)
	@XmlElementWrapper(name="items")
	@XmlElement(name="item")
	private Set<Item>items;
	
	@Column(name="createddate")
	@XmlElement(name="createddate")
	private Timestamp createddate;
	
	@Version
	@Column(name="updateddate")
	@XmlElement(name="updateddate")
	private Timestamp updateddate;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Post getPost() {
		return post;
	}
	public void setPost(Post post) {
		this.post = post;
	}
	public Member getAuthor() {
		return author;
	}
	public void setAuthor(Member author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public Set<Item> getItems() {
		return items;
	}
	public void setItems(Set<Item> items) {
		this.items = items;
	}
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public Timestamp getUpdateddate() {
		return updateddate;
	}
	public void setUpdateddate(Timestamp updateddate) {
		this.updateddate = updateddate;
	}
	public Comment() {}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(FILL_LINE).append(NEW_LINE)
			  .append("ID").append(TEMPLATE_ARROWS).append(this.getId()).append(NEW_LINE)
			  .append("TITLE").append(TEMPLATE_ARROWS).append(this.getTitle()).append(NEW_LINE)
			  .append("DISCRIPTION").append(TEMPLATE_ARROWS).append(this.getDiscription()).append(NEW_LINE)
			  .append("ITEMS").append(TEMPLATE_ARROWS).append(this.getItems()).append(NEW_LINE)
			  .append("CREATED").append(TEMPLATE_ARROWS).append(this.getCreateddate()).append(NEW_LINE)
			  .append("UPDATED").append(TEMPLATE_ARROWS).append(this.getUpdateddate()).append(NEW_LINE);
		return buffer.toString();
	}
}
