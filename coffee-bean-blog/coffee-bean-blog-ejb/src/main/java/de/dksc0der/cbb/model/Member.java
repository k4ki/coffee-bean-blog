package de.dksc0der.cbb.model;

import static de.dksc0der.cbb.util.AppConstants.FILL_LINE;
import static de.dksc0der.cbb.util.AppConstants.NEW_LINE;
import static de.dksc0der.cbb.util.AppConstants.TEMPLATE_ARROWS;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import de.dksc0der.cbb.model.id.MemberPK;
import de.dksc0der.cbb.util.AppConstants;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Entity
@IdClass(MemberPK.class)
@Table(name="member")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="member")
@NamedQueries({
	@NamedQuery(name=Member.FIND_BY_EMAIL, query="SELECT m FROM Member m WHERE m.email=:email"),
	@NamedQuery(name=Member.FIND_BY_TOKEN, query="SELECT m FROM Member m WHERE m.username=:username AND m.email=:email AND m.createdChecksum=:createdChecksum AND m.checksum=:checksum"),
	@NamedQuery(name=Member.COUNT_BY_USERNAME, query="SELECT COUNT(m) FROM Member m WHERE m.username=:username"),
	@NamedQuery(name=Member.COUNT_BY_EMAIL, query="SELECT COUNT(m) FROM Member m WHERE m.email=:email"),
	@NamedQuery(name=Member.COUNT_ALL, query="SELECT COUNT(m) FROM Member m"),
	@NamedQuery(name=Member.FIND_ALL, query="SELECT m FROM Member m")
})
public class Member implements Serializable{

	private static final long serialVersionUID = 1L;

	public final static String FIND_BY_EMAIL	 = "member.findByEmail";
	public final static String FIND_BY_TOKEN	 = "member.findByToken";
	public final static String COUNT_BY_USERNAME = "member.countByUsername";
	public final static String COUNT_BY_EMAIL    = "member.countByEmail";
	public final static String COUNT_ALL 		 = "member.CountAll";
	public final static String FIND_ALL 		 = "member.findAll";

	// ######################### SECURITY ELEMENTS ##########################
	@Id
	@Column(name="username")
	@XmlElement(name="username")
	private String username;
	
	@Column(name="email", unique=true)
	@XmlElement(name="email")
	private String email;
	
	@Column(name="password")
	@XmlTransient
	private String password;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinTable(name = "member_roles", 
		joinColumns = {
			@JoinColumn(name = "username", referencedColumnName = "username") }, 
		inverseJoinColumns = {
			@JoinColumn(name = "rolename", referencedColumnName = "rolename") })
	@XmlElementWrapper(name = "roles")
	@XmlElement(name="role")
	private Set<Role>roles;
	
	@Column(name="sessionid")
	@XmlElement(name="sessionid")
	private String sessionid;
	
	@Column(name="active")
	@XmlElement(name="active")
	private boolean active;
	
	@Column(name="createddate")
	@XmlElement(name="createddate")
	private Timestamp createddate;
	
	@Column(name="lastlogin")
	@XmlElement(name="lastLogin")
	private Timestamp lastLogin;
	
	@Column(name="lastlogout")
	@XmlElement(name="lastLogout")
	private Timestamp lastLogout;
	
	@Version
	@Column(name="updateddate")
	@XmlElement(name="updateddate")
	private Timestamp updateddate;
	
	// ####################### VERIFICATION ELEMENTS ########################
	
	@Column(name="verified")
	@XmlElement(name="verified")
	private boolean verified;
	
	@Column(name="checksum")
	@XmlElement(name="checksum")
	private String checksum;
	
	@Column(name="createdchecksum")
	@XmlElement(name="createdchecksum")
	private long createdChecksum;
	
	// ####################### MEMBERS INFO ELEMENTS ########################

	@Column(name="avatarpath")
	@XmlElement(name="avatarpath")
	private String avatarpath;
	
	@Column(name="firstname")
	@XmlElement(name="firstname")
	private String firstname;
	
	@Column(name="lastname")
	@XmlElement(name="lastname")
	private String lastname;
	
	@Column(name="birthday")
	@XmlElement(name="birthday")
	private Date birthday;
	
	@Lob
	@Column(name="discription")
	@XmlElement(name="discription")
	private String discription;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinTable(name = "member_categories", 
		joinColumns = {
			@JoinColumn(name = "username", referencedColumnName = "username") }, 
		inverseJoinColumns = {
			@JoinColumn(name = "categoryname", referencedColumnName = "categoryname") })
	@XmlElementWrapper(name = "categories")
	@XmlElement(name="category")
	private Set<Category>categories;
	
//	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
//	@JoinTable(name = "member_friends", 
//		joinColumns = {
//			@JoinColumn(name = "username", referencedColumnName = "username") }, 
//		inverseJoinColumns = {
//			@JoinColumn(name = "friend", referencedColumnName = "username") })
//	private Set<Member> friends;
	
	@OneToMany(mappedBy = "author", fetch = FetchType.EAGER, orphanRemoval = false)
	@XmlTransient
	private Set<Post>posts;
	
	@OneToMany(mappedBy = "author", fetch = FetchType.EAGER, orphanRemoval = false)
	@XmlTransient
	private Set<Comment> comments;
	
	//########################################################################
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Set<Role> getRoles() {
		if(roles == null)
			roles = new CopyOnWriteArraySet<Role>();
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	public String getSessionid() {
		return sessionid;
	}
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public Timestamp getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Timestamp lastLogin) {
		this.lastLogin = lastLogin;
	}
	public Timestamp getLastLogout() {
		return lastLogout;
	}
	public void setLastLogout(Timestamp lastLogout) {
		this.lastLogout = lastLogout;
	}
	public Timestamp getUpdateddate() {
		return updateddate;
	}
	public void setUpdateddate(Timestamp updateddate) {
		this.updateddate = updateddate;
	}
	public boolean isVerified() {
		return verified;
	}
	public void setVerified(boolean verified) {
		this.verified = verified;
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	public long getCreatedChecksum() {
		return createdChecksum;
	}
	public void setCreatedChecksum(long createdChecksum) {
		this.createdChecksum = createdChecksum;
	}
	public String getAvatarpath() {
		return avatarpath;
	}
	public void setAvatarpath(String avatarpath) {
		this.avatarpath = avatarpath;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public Set<Category> getCategories() {
		if(categories == null)
			categories = new CopyOnWriteArraySet<Category>();
		return categories;
	}
	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}
//	public Set<Member> getFriends() {
//		if(friends == null)
//			friends = new CopyOnWriteArraySet<Member>();
//		return friends;
//	}
//	public void setFriends(Set<Member> friends) {
//		this.friends = friends;
//	}
	public Set<Post> getPosts() {
		if(posts == null)
			posts = new CopyOnWriteArraySet<Post>();
		return posts;
	}
	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}
	public Set<Comment> getComments() {
		if(comments == null)
			comments = new CopyOnWriteArraySet<Comment>();
		return comments;
	}
	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}
	
	public Member() {}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(FILL_LINE).append(NEW_LINE)
			  .append("USERNAME").append(TEMPLATE_ARROWS).append(this.getUsername()).append(NEW_LINE)
			  .append("EMAIL").append(TEMPLATE_ARROWS).append(this.getEmail()).append(NEW_LINE)
			  .append("PASSWORD").append(TEMPLATE_ARROWS).append(this.getPassword()).append(NEW_LINE)
			  .append("ROLES").append(TEMPLATE_ARROWS).append(this.getRoles()).append(NEW_LINE)
			  .append("SESSIONID").append(TEMPLATE_ARROWS).append(this.getSessionid()).append(NEW_LINE)
			  .append("ISACTIVE").append(TEMPLATE_ARROWS).append(this.isActive()).append(NEW_LINE)
			  .append("CREATED").append(TEMPLATE_ARROWS).append(this.getCreateddate()).append(NEW_LINE)
			  .append("LASTLOGIN").append(TEMPLATE_ARROWS).append(this.getLastLogin()).append(NEW_LINE)
			  .append("UPDATED").append(TEMPLATE_ARROWS).append(this.getUpdateddate()).append(NEW_LINE)
			  .append("ISVERIFIED").append(TEMPLATE_ARROWS).append(this.isVerified()).append(NEW_LINE)
			  .append("CHECKSUM").append(TEMPLATE_ARROWS).append(this.getChecksum()).append(NEW_LINE)
			  .append("CREATEDCHECKSUM").append(TEMPLATE_ARROWS).append(this.getCreatedChecksum()).append(NEW_LINE)
			  .append("AVATARPATH").append(TEMPLATE_ARROWS).append(this.getAvatarpath()).append(NEW_LINE)
			  .append("FIRSTNAME").append(TEMPLATE_ARROWS).append(this.getFirstname()).append(NEW_LINE)
			  .append("LASTNAME").append(TEMPLATE_ARROWS).append(this.getLastname()).append(NEW_LINE)
			  .append("BIRTHDAY").append(TEMPLATE_ARROWS).append(this.getBirthday()).append(NEW_LINE)
			  .append("DISCRIPTION").append(TEMPLATE_ARROWS).append(this.getDiscription()).append(NEW_LINE)
			  .append("CATEGORIES").append(TEMPLATE_ARROWS).append(this.getCategories()).append(NEW_LINE)
//			  .append("FRIENDS").append(TEMPLATE_ARROWS).append(this.getFriends()).append(NEW_LINE)
			  .append("Posts").append(TEMPLATE_ARROWS).append(this.getPosts()).append(NEW_LINE);
		return buffer.toString();
	}
	
	public boolean isAdmin(){
		return findRoleByName(AppConstants.ROLE_ADMIN);
	}
	
	public boolean isMemberVerified(){
		return findRoleByName(AppConstants.ROLE_MEMBER_VERIFIED);
	}
	
	public boolean isMemberUnverified(){
		return findRoleByName(AppConstants.ROLE_MEMBER_UNVERIFIED);
	}
	
	private boolean findRoleByName(String rolename){
		boolean isInRole = false;
		Iterator<Role>itr = this.getRoles().iterator();
		while (itr.hasNext()) {
			Role role = (Role) itr.next();
			if(role.getRolename().equals(rolename)){
				isInRole = true;
				break;
			}
		}
		return isInRole;
	}
}
