package de.dksc0der.cbb.util;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
public class Resources {
	@Produces
	@PersistenceContext
	private EntityManager em;
}
