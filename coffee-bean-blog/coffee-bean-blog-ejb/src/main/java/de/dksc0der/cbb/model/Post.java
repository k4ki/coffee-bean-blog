package de.dksc0der.cbb.model;

import static de.dksc0der.cbb.util.AppConstants.FILL_LINE;
import static de.dksc0der.cbb.util.AppConstants.NEW_LINE;
import static de.dksc0der.cbb.util.AppConstants.TEMPLATE_ARROWS;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Entity
@Table(name = "post")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "post")
@NamedQueries({
		@NamedQuery(name = Post.FIND_BY_CATEGORY, query = "SELECT distinct p FROM Post p INNER JOIN p.categories c WHERE c.categoryname=:categoryname"),
		@NamedQuery(name = Post.FIND_BY_USERNAME, query = "SELECT p FROM Post p WHERE p.author.username=:username"),
		@NamedQuery(name = Post.FIND_ALL, query = "SELECT p FROM Post p") })
public class Post implements Serializable {

	private static final long serialVersionUID = 1L;

	public final static String FIND_BY_CATEGORY = "post.findByCategory";
	public final static String FIND_BY_USERNAME = "post.findByUsername";
	public final static String FIND_ALL = "post.findAll";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@XmlElement(name = "id")
	private long id;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinColumn(name = "author", referencedColumnName = "username")
	@XmlElement(name = "author")
	private Member author;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "post_categories", joinColumns = {
			@JoinColumn(name = "post_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "categoryname", referencedColumnName = "categoryname") })
	@XmlElementWrapper(name = "categories")
	@XmlElement(name = "category")
	private Set<Category> categories;

	@OneToMany(mappedBy = "post", fetch = FetchType.EAGER, orphanRemoval = true)
	@XmlElementWrapper(name = "comments")
	@XmlElement(name = "comment")
	private Set<Comment> comments;

	@OneToMany(mappedBy = "post", fetch = FetchType.EAGER, orphanRemoval = true)
	@XmlElementWrapper(name = "items")
	@XmlElement(name = "item")
	private Set<Item> items;

	@Column(name = "title")
	@XmlElement(name = "title")
	private String title;

	@Lob
	@Column(name = "discription")
	@XmlElement(name = "discription")
	private String discription;

	@Lob
	@Column(name = "content")
	@XmlElement(name = "content")
	private String content;

	@Column(name = "createddate")
	@XmlElement(name = "createddate")
	private Timestamp createddate;

	@Version
	@Column(name = "updateddate")
	@XmlElement(name = "updateddate")
	private Timestamp updateddate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Member getAuthor() {
		return author;
	}

	public void setAuthor(Member author) {
		this.author = author;
	}

	public Set<Category> getCategories() {
		if (categories == null)
			categories = new CopyOnWriteArraySet<Category>();
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public Set<Comment> getComments() {
		if (comments == null)
			comments = new CopyOnWriteArraySet<Comment>();
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Set<Item> getItems() {
		if (items == null)
			items = new CopyOnWriteArraySet<Item>();
		return items;
	}

	public void setItems(Set<Item> items) {
		this.getItems().clear();
		if (items != null) {
			this.getItems().addAll(items);
		}
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String discription) {
		this.discription = discription;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}

	public Timestamp getUpdateddate() {
		return updateddate;
	}

	public void setUpdateddate(Timestamp updateddate) {
		this.updateddate = updateddate;
	}

	public Post() {
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(FILL_LINE).append(NEW_LINE).append("ID").append(TEMPLATE_ARROWS).append(this.getId())
				.append(NEW_LINE).append("TITLE").append(TEMPLATE_ARROWS).append(this.getTitle()).append(NEW_LINE)
				.append("DISCRIPTION").append(TEMPLATE_ARROWS).append(this.getDiscription()).append(NEW_LINE)
				.append("CATEGORIES").append(TEMPLATE_ARROWS).append(this.getCategories()).append(NEW_LINE)
				.append("ITEMS").append(TEMPLATE_ARROWS).append(this.getItems()).append(NEW_LINE).append("CREATED")
				.append(TEMPLATE_ARROWS).append(this.getCreateddate()).append(NEW_LINE).append("UPDATED")
				.append(TEMPLATE_ARROWS).append(this.getUpdateddate()).append(NEW_LINE);
		return buffer.toString();
	}
}
