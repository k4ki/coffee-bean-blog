package de.dksc0der.cbb.service;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.jboss.ejb3.annotation.SecurityDomain;
import org.jboss.logging.Logger;

import de.dksc0der.cbb.model.Role;
import de.dksc0der.cbb.repository.MemberRoleService;
import de.dksc0der.cbb.util.AppConstants;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Singleton
@SecurityDomain("coffee-bean-blog")
public class MemberRoleServiceImpl implements MemberRoleService {

	private static Logger log = Logger.getLogger(MemberRoleServiceImpl.class.getName());
	
	@Inject
	private EntityManager em;
	
	@Override
	@PermitAll
	public Set<Role> findAll() {
		log.info("CLASS::MemberRoleServiceImpl --> findAll::BEGIN");
		Set<Role>roles = new CopyOnWriteArraySet<Role>(em.createNamedQuery(Role.FIND_ALL, Role.class).getResultList());
		//log.info("CLASS::MemberRoleServiceImpl --> findAll -->> RESULT(ROLES) =  \n" + roles);
		log.info("CLASS::MemberRoleServiceImpl --> findAll::END");
		return roles;
	}

	@Override
	@RolesAllowed(AppConstants.ROLE_ADMIN)
	public Role getAdminRole() {
		log.info("CLASS::MemberRoleServiceImpl --> getAdminRole::BEGIN");
		Set<Role>roles = findAll();
		Iterator<Role>itrRole = roles.iterator();
		while (itrRole.hasNext()) {
			Role role = (Role) itrRole.next();
			if(role.getRolename().equals(AppConstants.ROLE_ADMIN)){
				//log.info("CLASS::MemberRoleServiceImpl --> getAdminRole -->> RESULT(ROLE) =  \n" + role);
				log.info("CLASS::MemberRoleServiceImpl --> getAdminRole::END");
				return role;
			}
		}
		log.info("CLASS::MemberRoleServiceImpl --> saveAndEncode::END");
		throw new RuntimeException("Role ADMIN not found"); 
	}

	@Override
	@RolesAllowed({AppConstants.ROLE_ADMIN, AppConstants.ROLE_MEMBER_UNVERIFIED})
	public Role getMemberVerifiedRole() {
		log.info("CLASS::MemberRoleServiceImpl --> getMemberVerifiedRole::BEGIN");
		Set<Role>roles = findAll();
		Iterator<Role>itrRole = roles.iterator();
		while (itrRole.hasNext()) {
			Role role = (Role) itrRole.next();
			if(role.getRolename().equals(AppConstants.ROLE_MEMBER_VERIFIED)){
				log.info("CLASS::MemberRoleServiceImpl --> getMemberVerifiedRole -->> RESULT(ROLE) =  \n" + role);
				//log.info("CLASS::MemberRoleServiceImpl --> getMemberVerifiedRole::END");
				return role;
			}
		}
		log.info("CLASS::MemberRoleServiceImpl --> getMemberVerifiedRole::END");
		throw new RuntimeException("Role MEMBER VERIFIED not found"); 
	}

	@Override
	@PermitAll
	public Role getMemberUnVerifiedRole() {
		log.info("CLASS::MemberRoleServiceImpl --> getMemberUnVerifiedRole::BEGIN");
		Set<Role>roles = findAll();
		Iterator<Role>itrRole = roles.iterator();
		while (itrRole.hasNext()) {
			Role role = (Role) itrRole.next();
			if(role.getRolename().equals(AppConstants.ROLE_MEMBER_UNVERIFIED)){
				//log.info("CLASS::MemberRoleServiceImpl --> getMemberUnVerifiedRole -->> RESULT(ROLE) =  \n" + role);
				log.info("CLASS::MemberRoleServiceImpl --> getMemberUnVerifiedRole::END");
				return role;
			}
		}
		log.info("CLASS::MemberRoleServiceImpl --> getMemberUnVerifiedRole::END");
		throw new RuntimeException("Role MEMBER UNVERIFIED not found"); 
	}

}
