package de.dksc0der.cbb.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
public class EncodeDecodeUtil {
	
	private static final int 	SALT 	= 36;
	private static final String REGEX 	= "::";
	
	public static String encode(String plain) throws NoSuchAlgorithmException {
		byte[] hash = MessageDigest.getInstance("SHA-256").digest(plain.getBytes());    
		return DatatypeConverter.printBase64Binary(hash);
	}

	private static String encodeBase64Shift(String plain) {
		StringBuilder msg = new StringBuilder(plain);
		for (int i = 0; i < msg.length(); i ++) {
		    msg.setCharAt(i, (char) (msg.charAt(i) + EncodeDecodeUtil.SALT));
		}
		
		return DatatypeConverter.printBase64Binary(msg.toString().getBytes());
	}

	private static String decodeBase64Shift(String plain) {
		StringBuilder msg = new StringBuilder(new String(DatatypeConverter.parseBase64Binary(plain)));
		for (int i = 0; i < msg.length(); i ++) {
		    msg.setCharAt(i, (char) (msg.charAt(i) - EncodeDecodeUtil.SALT));
		}
		return msg.toString();
	}
	
	public static String encodeToLinkParameter(String username, 
										String email, 
										Long checksumCreated, 
										String checksum){
		String clean = username + REGEX + email + REGEX + checksumCreated + REGEX + checksum;
		
		return encodeBase64Shift(clean);
	}
	
	public static String[] decodeToValues(String token){
		String clean = decodeBase64Shift(token);
		return clean.split(REGEX);
	}
}
