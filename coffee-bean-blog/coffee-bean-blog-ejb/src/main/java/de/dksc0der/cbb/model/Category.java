package de.dksc0der.cbb.model;

import static de.dksc0der.cbb.util.AppConstants.FILL_LINE;
import static de.dksc0der.cbb.util.AppConstants.NEW_LINE;
import static de.dksc0der.cbb.util.AppConstants.TEMPLATE_ARROWS;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import de.dksc0der.cbb.model.id.CategoryPK;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Entity
@IdClass(CategoryPK.class)
@Table(name="category")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="category")
@NamedQueries({
	@NamedQuery(name=Category.FIND_ALL, query="SELECT c FROM Category c")
})
public class Category implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public final static String FIND_ALL = "category.findAll";

	@Id
	@Column(name="categoryname")
	@XmlElement(name="categoryname")
	private String categoryname;
	
	@Version
	@Column(name="updateddate")
	@XmlElement(name="updateddate")
	private Timestamp updateddate;
	
	@ManyToMany(mappedBy="categories",fetch=FetchType.EAGER)
	@XmlTransient
	private Set<Post>posts;

	public String getCategoryname() {
		return categoryname;
	}

	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}

	public Timestamp getUpdateddate() {
		return updateddate;
	}

	public void setUpdateddate(Timestamp updateddate) {
		this.updateddate = updateddate;
	}
	
	public Category() {}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(FILL_LINE).append(NEW_LINE)
			  .append("CATEGORYNAME").append(TEMPLATE_ARROWS).append(this.getCategoryname()).append(NEW_LINE)
			  .append("UPDATED").append(TEMPLATE_ARROWS).append(this.getUpdateddate()).append(NEW_LINE);
		return buffer.toString();
	}
}
