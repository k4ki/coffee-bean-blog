package de.dksc0der.cbb.service;

import java.sql.Timestamp;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.jboss.ejb3.annotation.SecurityDomain;
import org.jboss.logging.Logger;

import de.dksc0der.cbb.model.Category;
import de.dksc0der.cbb.model.Item;
import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.model.Post;
import de.dksc0der.cbb.model.id.MemberPK;
import de.dksc0der.cbb.repository.CategoryService;
import de.dksc0der.cbb.repository.ItemService;
import de.dksc0der.cbb.repository.MemberPostService;
import de.dksc0der.cbb.util.AppConstants;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Stateless
@SecurityDomain("coffee-bean-blog")
public class MemberPostServiceImpl implements MemberPostService {
	
	private static Logger log = Logger.getLogger(MemberPostServiceImpl.class.getName());
	
	@Inject
	private EntityManager em;
	
	@EJB
	private CategoryService categoryService;
	
	@EJB
	private ItemService itemService;
	
	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Post create(Member member, Post post) {
		log.info("CLASS::MemberPostServiceImpl --> create::BEGIN");
		member = findMemberByUsername(member.getUsername());
		post.setItems(null);
		post.setCreateddate(new Timestamp(System.currentTimeMillis()));
		post.setAuthor(member);
		log.info("CLASS::MemberPostServiceImpl --> create::END");
		return em.merge(post);
	}

	
	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Post updateCategory(Post post, Category category) {
		log.info("CLASS::MemberPostServiceImpl --> create::BEGIN");
		category = categoryService.create(category);
		//log.info("CLASS::MemberPostServiceImpl --> updateCategory -->> RESULT(category) =  " + category);
		post = findPostById(post.getId());
		post.getCategories().add(em.merge(category));
		log.info("CLASS::MemberPostServiceImpl --> updateCategory::END");
		return em.merge(post);
	}


	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Post deleteCategory(Post post, Category category) {
		log.info("CLASS::MemberPostServiceImpl --> deleteCategory::BEGIN");
		post = findPostById(post.getId());
		Iterator<Category>itrCats = post.getCategories().iterator();
		while (itrCats.hasNext()) {
			Category deleteCategaory = (Category) itrCats.next();
			if(category.getCategoryname().equals(deleteCategaory.getCategoryname()))
				itrCats.remove();
		}
		log.info("CLASS::MemberPostServiceImpl --> deleteCategory::END");
		return em.merge(post);
	}


	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Post updateItem(Post post, Item item) {
		log.info("CLASS::MemberPostServiceImpl --> updateItem::BEGIN");
		item = itemService.create(item);
		post = findPostById(post.getId());
		post.getItems().add(item);
		post = em.merge(post);
		log.info("CLASS::MemberPostServiceImpl --> updateItem::END");
		return em.merge(post);
	}


	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Post deleteItem(Post post, Item item) {
		log.info("CLASS::MemberPostServiceImpl --> deleteItem::BEGIN");
		post = findPostById(post.getId());
		Iterator<Item>itrItems = post.getItems().iterator();
		while (itrItems.hasNext()) {
			Item deleteItem = (Item) itrItems.next();
			if(deleteItem.getId() == item.getId())
				itrItems.remove();
			
		}
		log.info("CLASS::MemberPostServiceImpl --> deleteItem::END");
		return em.merge(post);
	}


	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public void delete(Post post) {
		log.info("CLASS::MemberPostServiceImpl --> delete::BEGIN");
		post = findPostById(post.getId());
		em.remove(post);
		log.info("CLASS::MemberPostServiceImpl --> delete::END");
	}

	@Override
	@PermitAll
	public Post findPostById(long id) {
		log.info("CLASS::MemberPostServiceImpl --> findPostById::BEGIN");
		Post post = em.find(Post.class, id);
		//log.info("CLASS::MemberPostServiceImpl --> findPostById -->> RESULT(POST) =  \n" + post);
		log.info("CLASS::MemberPostServiceImpl --> findPostById::END");
		return post;
	}

	@Override
	@PermitAll
	public Set<Post> findAll() {
		log.info("CLASS::MemberPostServiceImpl --> findAll::BEGIN");
		Set<Post>posts = new CopyOnWriteArraySet<Post>(em.createNamedQuery(Post.FIND_ALL, Post.class).getResultList());
		//log.info("CLASS::MemberPostServiceImpl --> findAll -->> RESULT(POSTS) =  \n" + posts);
		log.info("CLASS::MemberPostServiceImpl --> findAll::END");
		return posts;
	}

	@Override
	@PermitAll
	public Set<Post> findPostByCategory(Category category) {
		log.info("CLASS::MemberPostServiceImpl --> findPostByCategory::BEGIN");
		Set<Post>posts = new CopyOnWriteArraySet<Post>(em.createNamedQuery(Post.FIND_BY_CATEGORY, Post.class)
					 .setParameter("categoryname", category.getCategoryname())
					 .getResultList());
		//log.info("CLASS::MemberPostServiceImpl --> findPostByCategory -->> RESULT(POSTS) =  \n" + posts);
		log.info("CLASS::MemberPostServiceImpl --> findPostByCategory::END");
		return posts;
	}

	@Override
	@PermitAll
	public Set<Post> findPostByUsername(String username) {
		log.info("CLASS::MemberPostServiceImpl --> findPostByUsername::BEGIN");
		Set<Post>posts = new CopyOnWriteArraySet<Post>(em.createNamedQuery(Post.FIND_BY_USERNAME, Post.class)
				   										 .setParameter("username", username)
				   										 .getResultList());
		//log.info("CLASS::MemberPostServiceImpl --> findPostByUsername -->> RESULT(POSTS) =  \n" + posts);
		log.info("CLASS::MemberPostServiceImpl --> findPostByUsername::END");
		return posts;
	}

	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Member findMemberByUsername(String username) {
		log.info("CLASS::MemberPostServiceImpl --> findMemberByUsername::BEGIN");
		MemberPK memberPK = new MemberPK();
		memberPK.setUsername(username);
		Member member = em.find(Member.class, memberPK);
		//log.info("CLASS::MemberPostServiceImpl --> findMemberByUsername -->> RESULT(MEMBER) =  \n" + member);
		log.info("CLASS::MemberPostServiceImpl --> findMemberByUsername::END");
		return member;
	}
}
