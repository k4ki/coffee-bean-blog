package de.dksc0der.cbb.util;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
public class AppConstants {
	
	// ####################### CHAR CONSTANTS ################################
	
	public final static String NEW_LINE 		= "\n";
	public final static String TAB_LINE 		= "\t";
	public final static String FILL_LINE 		= "---------------------------------------------";
	public final static String TEMPLATE_ARROWS 	= " -->> ";
	
	// ####################### ROLE CONSTANTS ################################
	
	public final static String ROLE_ADMIN 					= "ADMIN"; 
	public final static String ROLE_MEMBER_VERIFIED			= "MEMBER_VERIFIED"; 
	public final static String ROLE_MEMBER_UNVERIFIED 		= "MEMBER_UNVERIFIED"; 
	
}
