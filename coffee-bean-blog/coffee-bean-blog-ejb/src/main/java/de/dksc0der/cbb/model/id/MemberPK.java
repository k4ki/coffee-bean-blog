package de.dksc0der.cbb.model.id;

import java.io.Serializable;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
public class MemberPK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public MemberPK() {}

	@Override
	public int hashCode() {
		return (int)username.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean equal = false;
		if(obj instanceof RolePK){
			MemberPK memberPK = (MemberPK) obj;
			if(memberPK.getUsername().equals(this.getUsername())){
				equal = true;
			}
		}
		return equal;
	}
}
