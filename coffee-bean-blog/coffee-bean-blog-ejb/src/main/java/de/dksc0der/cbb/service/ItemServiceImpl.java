package de.dksc0der.cbb.service;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.jboss.ejb3.annotation.SecurityDomain;
import org.jboss.logging.Logger;

import de.dksc0der.cbb.model.Item;
import de.dksc0der.cbb.repository.ItemService;
import de.dksc0der.cbb.util.AppConstants;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Stateless
@SecurityDomain("coffee-bean-blog")
public class ItemServiceImpl implements ItemService {

	private static Logger log = Logger.getLogger(ItemServiceImpl.class.getName());
	
	@Inject
	private EntityManager em;
	
	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Item create(Item item) {
		log.info("CLASS::ItemServiceImpl --> create::BEGIN");
		item = em.merge(item);
		em.flush();
		//log.info("CLASS::ItemServiceImpl --> create -->> RESULT(ITEM) =  \n" + item);
		log.info("CLASS::ItemServiceImpl --> create::END");
		return item;
	}

	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public Item update(Item item) {
		log.info("CLASS::ItemServiceImpl --> update::BEGIN");
		Item updatreItem = findByID(item.getId());
		updatreItem.setContent(item.getContent());
		updatreItem = em.merge(updatreItem);
		//log.info("CLASS::ItemServiceImpl --> update -->> RESULT(ITEM) =  \n" + item);
		log.info("CLASS::ItemServiceImpl --> update::END");
		return updatreItem;
	}

	@Override
	@RolesAllowed(AppConstants.ROLE_MEMBER_VERIFIED)
	public void delete(Item item) {
		log.info("CLASS::ItemServiceImpl --> delete::BEGIN");
		item = findByID(item.getId());
		em.remove(item);
		em.flush();
		//log.info("CLASS::ItemServiceImpl --> delete -->> RESULT(ITEM) =  \n" + item);
		log.info("CLASS::ItemServiceImpl --> delete::END");
	}

	@Override
	@PermitAll
	public Item findByID(long id) {
		log.info("CLASS::ItemServiceImpl --> findByID::BEGIN");
		Item item = em.find(Item.class, id);
		//log.info("CLASS::ItemServiceImpl --> findByID -->> RESULT(ITEM) =  \n" + item);
		log.info("CLASS::ItemServiceImpl --> findByID::END");
		return item;
	}

}
