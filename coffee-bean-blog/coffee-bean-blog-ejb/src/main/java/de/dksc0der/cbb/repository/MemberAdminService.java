package de.dksc0der.cbb.repository;

import java.util.Set;

import javax.ejb.Remote;

import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.model.Role;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Remote
public interface MemberAdminService {
	Member promote(Member member, Role role);
	Member demote(Member member, Role role);
	
	Member findByUsername(String username);
	Set<Member> findAll();
}
