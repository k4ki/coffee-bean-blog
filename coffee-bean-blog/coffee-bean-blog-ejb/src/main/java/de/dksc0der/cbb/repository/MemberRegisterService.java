package de.dksc0der.cbb.repository;

import javax.ejb.Remote;

import de.dksc0der.cbb.model.Member;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Remote
public interface MemberRegisterService {
	Member saveAndEncode (Member member);
	long countAllMember();
}
