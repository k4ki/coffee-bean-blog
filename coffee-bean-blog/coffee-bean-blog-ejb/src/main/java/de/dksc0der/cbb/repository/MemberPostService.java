package de.dksc0der.cbb.repository;

import java.util.Set;

import javax.ejb.Remote;

import de.dksc0der.cbb.model.Category;
import de.dksc0der.cbb.model.Item;
import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.model.Post;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Remote
public interface MemberPostService {
	Post create(Member member, Post post);
	
	Post updateCategory(Post post, Category category);
	Post deleteCategory(Post post, Category category);
	
	Post updateItem(Post post, Item item);
	Post deleteItem(Post post, Item item);
	
	void delete(Post post);
	
	Post findPostById(long id);
	
	Set<Post> findAll();
	Set<Post> findPostByCategory(Category category);
	Set<Post> findPostByUsername(String username);
	
	Member findMemberByUsername(String username);
}
