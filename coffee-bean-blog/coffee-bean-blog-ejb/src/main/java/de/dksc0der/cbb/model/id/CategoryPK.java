package de.dksc0der.cbb.model.id;

import java.io.Serializable;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
public class CategoryPK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String categoryname;

	public String getCategoryname() {
		return categoryname;
	}

	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}
	
	@Override
	public int hashCode() {
		return (int) categoryname.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean equal = false;
		if(obj instanceof RolePK){
			CategoryPK categoryPK = (CategoryPK) obj;
			if(categoryPK.getCategoryname().equals(this.getCategoryname())){
				equal = true;
			}
		}
		return equal;
	}
}
