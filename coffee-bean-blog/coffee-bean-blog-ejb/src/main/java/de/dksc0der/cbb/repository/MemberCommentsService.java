package de.dksc0der.cbb.repository;

import java.util.Set;

import javax.ejb.Remote;

import de.dksc0der.cbb.model.Comment;
import de.dksc0der.cbb.model.Item;
import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.model.Post;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@Remote
public interface MemberCommentsService {
	Comment create(Member member, Post post, Comment comment);
	void delete(Comment comment);
	
	Comment updateItem(Comment comment, Item item);
	Comment deleteItem(Comment comment, Item item);
	
	Comment findCommentById(long id);
	Set<Comment> findCommentsByUsername(String username);
	Set<Comment> findCommentByPost(Post post);
	
	Post findPostByID(long id);
	Member findMemberByUsername(String username);
}
