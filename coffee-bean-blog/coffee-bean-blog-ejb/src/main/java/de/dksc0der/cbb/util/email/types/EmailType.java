package de.dksc0der.cbb.util.email.types;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
public enum EmailType {
	VERIFICATION("verification"), PASSWORD_RECOVERY("password");
	
	private String typename;
	
	EmailType(String typename){
		this.setTypename(typename);
	}

	public String getTypename() {
		return typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}
	
}
