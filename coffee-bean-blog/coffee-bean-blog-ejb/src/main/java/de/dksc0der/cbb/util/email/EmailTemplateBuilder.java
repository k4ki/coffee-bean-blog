package de.dksc0der.cbb.util.email;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.Future;

import javax.ejb.AsyncResult;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Base64;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.Message;

import de.dksc0der.cbb.util.email.types.EmailType;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
public class EmailTemplateBuilder {

	private static final String RESOURCEBUNDLE = "languages/messages";

	private ResourceBundle bundle;
	
	public ResourceBundle getBundle() {
		return bundle;
	}

	public void setBundle(ResourceBundle bundle) {
		this.bundle = bundle;
	}

	public EmailTemplateBuilder(Locale locale) {
		bundle = ResourceBundle.getBundle(EmailTemplateBuilder.RESOURCEBUNDLE, locale);
	}
	
	public String createEmailBodyByTemplate(EmailType emailType, String token, Locale locale)
			throws IOException {
		InputStream is = EmailTemplateBuilder.class.getClassLoader()
				.getResourceAsStream("templates/email_template.html");
		String template = "";
		int content;
		while ((content = is.read()) != -1) {
			template += (char) content;
		}
		template = template.replace("{title}", bundle.getString(emailType.getTypename() + ".title"));
		template = template.replace("{bodyText}", bundle.getString(emailType.getTypename() + ".body"));
		template = template.replace("{btnText}", bundle.getString(emailType.getTypename() + ".button"));
		template = template.replace("{address}", bundle.getString(emailType.getTypename() + ".address"));
		template = template.replace("{token}", token);
		return template;
	}

	public String createEmailSubject(EmailType emailType, Locale locale) {
		return bundle.getString(emailType.getTypename() + ".subject");
	}

	private static Message createMessageWithEmail(MimeMessage mime) throws MessagingException, IOException {
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		mime.writeTo(buffer);
		byte[] bytes = buffer.toByteArray();
		String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
		Message message = new Message();
		message.setRaw(encodedEmail);
		return message;
	}

	private static MimeMessage createEmail(String to, String from, String subject, String bodyText, boolean isHtml)
			throws Exception {
		Properties props = new Properties();
		Session session = Session.getInstance(props, null);
		session.setDebug(true);
		MimeMessage mail = new MimeMessage(session);

		mail.setRecipients(RecipientType.TO, InternetAddress.parse(to));
		mail.setFrom(new InternetAddress(from));

		mail.setSubject(subject);

		if (isHtml) {
			mail.setText(bodyText, "utf-8", "html");
		} else {
			mail.setText(bodyText, "utf-8", "text");
		}
		return mail;
	}

	public Future<Boolean> sender(String to, String from, String subject, String bodyText, boolean isHtml) {
		boolean result = false;
		try {
			String USERHOME = System.getProperty("user.home");
			String SEPERATOR = System.getProperty("file.separator");
			String APPLICATION_NAME = "coffee-bean-blog";
			File DATA_STORE_DIR = new File(USERHOME + SEPERATOR + ".store/cbblog_secure");

			FileDataStoreFactory dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);

			HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
			JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();

			GoogleClientSecrets clientSecret = GoogleClientSecrets.load(jsonFactory, new InputStreamReader(
					EmailTemplateBuilder.class.getResourceAsStream("/client_secret.json")));

			GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, jsonFactory,
					clientSecret, Collections.singleton(GmailScopes.GMAIL_SEND)).setDataStoreFactory(dataStoreFactory)
							.setAccessType("offline").build();

			Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver())
					.authorize("user");

			Gmail service = new Gmail.Builder(httpTransport, jsonFactory, credential)
					.setApplicationName(APPLICATION_NAME).build();

			MimeMessage mail = createEmail(to, from, subject, bodyText, isHtml);

			Message msg = createMessageWithEmail(mail);

			msg = service.users().messages().send("me", msg).execute();

			result = true;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new AsyncResult<Boolean>(result);
	}
}
