INSERT INTO role (`rolename`, `updateddate`) VALUES ('ADMIN', NOW());
INSERT INTO role (`rolename`, `updateddate`) VALUES ('GUEST', NOW());
INSERT INTO role (`rolename`, `updateddate`) VALUES ('MEMBER_UNVERIFIED', NOW());
INSERT INTO role (`rolename`, `updateddate`) VALUES ('MEMBER_VERIFIED', NOW());

INSERT INTO category (`categoryname`, `updateddate`) VALUES ('JavaSE', NOW());
INSERT INTO category (`categoryname`, `updateddate`) VALUES ('JavaEE', NOW());
INSERT INTO category (`categoryname`, `updateddate`) VALUES ('JavaFX', NOW());
INSERT INTO category (`categoryname`, `updateddate`) VALUES ('JavaME', NOW());

INSERT INTO member (`username`, `email`, `password`, `active`, `createddate`, `lastlogin`, `updateddate`, `verified`, `avatarpath`, `firstname`, `lastname`, `birthday`, `discription`, `createdchecksum`) VALUES ('dksc0der', 'zus3iik4k@gmail.com', 'jGl25bVBBBW96Qi9Te4V37Fnqchz/Eu4qB9vKrRIqRg=', b'0', NOW(), NOW(), NOW(), b'1', 'image.png', 'David', 'Kunschke', '1982-07-10', 'Beschreibung', 0);
INSERT INTO member (`username`, `email`, `password`, `active`, `createddate`, `lastlogin`, `updateddate`, `verified`, `avatarpath`, `firstname`, `lastname`, `birthday`, `discription`, `createdchecksum`) VALUES ('dummy', 'davkun107@live.de', 'jGl25bVBBBW96Qi9Te4V37Fnqchz/Eu4qB9vKrRIqRg=', b'0', NOW(), NOW(), NOW(), b'0', 'image.png', 'Max', 'Mustermann', '1982-07-10', 'Beschreibung', 0);

INSERT INTO member_categories(`username`, `categoryname`) VALUES ('dksc0der', 'JavaEE');
INSERT INTO member_categories(`username`, `categoryname`) VALUES ('dksc0der', 'JavaSE');
INSERT INTO member_categories(`username`, `categoryname`) VALUES ('dksc0der', 'JavaFX');
INSERT INTO member_categories(`username`, `categoryname`) VALUES ('dummy', 'JavaME');

INSERT INTO member_roles (`username`, `rolename`) VALUES ('dksc0der', 'ADMIN');
INSERT INTO member_roles (`username`, `rolename`) VALUES ('dksc0der', 'MEMBER_VERIFIED'); 
INSERT INTO member_roles (`username`, `rolename`) VALUES ('dummy', 'MEMBER_UNVERIFIED'); 

INSERT INTO post (`id`, `title`, `discription`, `author`, `createddate`, `updateddate`)VALUES (1, 'My first post title by coffee-bean-blog', 'i can write on this shit', 'dksc0der', NOW(), NOW());
INSERT INTO post (`id`, `title`, `discription`, `author`, `createddate`, `updateddate`)VALUES (2, 'My second post title by coffee-bean-blog', 'i can write on this shit everytime', 'dksc0der', NOW(), NOW());
INSERT INTO post (`id`, `title`, `discription`, `author`, `createddate`, `updateddate`)VALUES (3, 'My first post title by coffee-bean-blog', 'i can write on this shit', 'dummy', NOW(), NOW());
INSERT INTO post (`id`, `title`, `discription`, `author`, `createddate`, `updateddate`)VALUES (4, 'My second post title by coffee-bean-blog', 'i can write on this shit, when ever i want', 'dummy', NOW(), NOW());

INSERT INTO post_categories(`post_id`, `categoryname`) VALUES (1, 'JavaSE');
INSERT INTO post_categories(`post_id`, `categoryname`) VALUES (1, 'JavaEE');
INSERT INTO post_categories(`post_id`, `categoryname`) VALUES (2, 'JavaME');
INSERT INTO post_categories(`post_id`, `categoryname`) VALUES (2, 'JavaSE');
INSERT INTO post_categories(`post_id`, `categoryname`) VALUES (3, 'JavaFX');
INSERT INTO post_categories(`post_id`, `categoryname`) VALUES (3, 'JavaEE');
INSERT INTO post_categories(`post_id`, `categoryname`) VALUES (4, 'JavaSE');
INSERT INTO post_categories(`post_id`, `categoryname`) VALUES (4, 'JavaFX');


INSERT INTO comment (`id`, `title`, `discription`, `post_id`, `author`, `createddate`, `updateddate`)VALUES (1, 'My first comment title by coffee-bean-blog', 'i can write on this shit', 1, 'dksc0der', NOW(), NOW());
INSERT INTO comment (`id`, `title`, `discription`, `post_id`, `author`, `createddate`, `updateddate`)VALUES (2, 'My second comment title by coffee-bean-blog', 'i can write on this shit everytime', 2, 'dksc0der', NOW(), NOW());
INSERT INTO comment (`id`, `title`, `discription`, `post_id`, `author`, `createddate`, `updateddate`)VALUES (3, 'My first comment title by coffee-bean-blog', 'i can write on this shit', 3, 'dummy', NOW(), NOW());
INSERT INTO comment (`id`, `title`, `discription`, `post_id`, `author`, `createddate`, `updateddate`)VALUES (4, 'My second comment title by coffee-bean-blog', 'i can write on this shit, when ever i want', 1, 'dummy', NOW(), NOW());

INSERT INTO item (`id`, `post_id`, `comment_id`, `content`, `updateddate`) VALUES (1, 1, null, 'Hello this is the content!!!', NOW());
INSERT INTO item (`id`, `post_id`, `comment_id`, `content`, `updateddate`) VALUES (2, 2, null, 'Hello this is the content!!!', NOW());
INSERT INTO item (`id`, `post_id`, `comment_id`, `content`, `updateddate`) VALUES (3, 3, null, 'Hello this is the content!!!', NOW());
INSERT INTO item (`id`, `post_id`, `comment_id`, `content`, `updateddate`) VALUES (4, 4, null, 'Hello this is the content!!!', NOW());
INSERT INTO item (`id`, `post_id`, `comment_id`, `content`, `updateddate`) VALUES (5, null, 1, 'Hello this is the content!!!', NOW());
INSERT INTO item (`id`, `post_id`, `comment_id`, `content`, `updateddate`) VALUES (6, null, 2, 'Hello this is the content!!!', NOW());
INSERT INTO item (`id`, `post_id`, `comment_id`, `content`, `updateddate`) VALUES (7, null, 3, 'Hello this is the content!!!', NOW());
INSERT INTO item (`id`, `post_id`, `comment_id`, `content`, `updateddate`) VALUES (8, null, 4, 'Hello this is the content!!!', NOW());


