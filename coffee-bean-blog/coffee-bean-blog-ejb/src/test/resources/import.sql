INSERT INTO role (`rolename`, `updatedate`) VALUES ('ADMIN', NOW());
INSERT INTO role (`rolename`, `updatedate`) VALUES ('GUEST', NOW());
INSERT INTO role (`rolename`, `updatedate`) VALUES ('MEMBER_UNVERIFIED', NOW());
INSERT INTO role (`rolename`, `updatedate`) VALUES ('MEMBER_VERIFIED', NOW());

INSERT INTO member (`username`, `avatarpath`, `birthday`, `createddate`, `discription`, `email`, `firstname`, `lastname`, password, `updatedate`, `verified`) VALUES ('dkscoder', '/images/image.png', '1982-07-10', NOW(), 'Beschreibung', 'davkun107@live.de', 'David', 'Kunschke', '12345678', NOW(), b'0');
INSERT INTO member (`username`, `avatarpath`, `birthday`, `createddate`, `discription`, `email`, `firstname`, `lastname`, password, `updatedate`, `verified`) VALUES ('dummy', '/image/imqage.png', '2017-01-24', NOW(), 'beschreibung', 'dummy@live.de', 'Dum', 'My', '12345678', NOW(), b'0');

INSERT INTO member_roles (`username`, `rolename`) VALUES ('dkscoder', 'ADMIN');
INSERT INTO member_roles (`username`, `rolename`) VALUES ('dkscoder', 'MEMBER_VERIFIED'); 
INSERT INTO member_roles (`username`, `rolename`) VALUES ('dummy', 'MEMBER_VERIFIED');

INSERT INTO sessionlog (`connectedip`, `sessionid`, `sessionstart`, `updatedate`, `username`) VALUES ('1.1.1.1', 'sessionid1', NOW(), NOW(), 'dkscoder');
INSERT INTO sessionlog (`connectedip`, `sessionid`, `sessionstart`, `updatedate`, `username`) VALUES ('2.2.2.2', 'sessionid2', NOW(), NOW(), 'dkscoder');
INSERT INTO sessionlog (`connectedip`, `sessionid`, `sessionstart`, `updatedate`, `username`) VALUES ('3.3.3.3', 'sessionid3', NOW(), NOW(), 'dummy');
INSERT INTO sessionlog (`connectedip`, `sessionid`, `sessionstart`, `updatedate`, `username`) VALUES ('4.4.4.4', 'sessionid4', NOW(), NOW(), 'dummy');

