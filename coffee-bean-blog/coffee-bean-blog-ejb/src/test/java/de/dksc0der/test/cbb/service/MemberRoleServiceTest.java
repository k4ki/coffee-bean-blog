package de.dksc0der.test.cbb.service;

import static org.junit.Assert.*;

import java.util.Set;
import java.util.concurrent.Callable;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.model.Role;
import de.dksc0der.cbb.repository.MemberRoleService;
import de.dksc0der.cbb.util.AppConstants;
import de.dksc0der.test.cbb.util.Admin;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@RunWith(Arquillian.class)
public class MemberRoleServiceTest {

	private static Logger log = Logger.getLogger(MemberRoleServiceTest.class.getName());

	@EJB
	private MemberRoleService roleService;
	
	@Inject
	private Admin admin;

	@Deployment
	public static Archive<?> createTestArchive() {
		log.info("TESTCASE::MemberRoleServiceTest --> createTestArchive");
		return ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackages(true, "de.dksc0der.cbb")
				.addPackages(true, "de.dksc0der.test.cbb.util")
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsResource("import.sql", "import.sql").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				// Deploy test datasource
				.addAsWebInfResource("test-ds.xml", "test-ds.xml");
	}

	@Test
	@InSequence(1)
	public void findAllTest() {
		log.info("TESTCASE::MemberRoleServiceTest --> findAll");
		Set<Role>roles = roleService.findAll();
		log.info("TESTCASE::MemberRoleServiceTest --> updateSecurityTest -->> RESULT(roles) =  " + roles);
		assertNotNull(roles);
	}

	@Test
	@InSequence(2)
	public void getAdminRoleTest() throws Exception {
		log.info("TESTCASE::MemberRoleServiceTest --> getAdminRoleTest");
		admin.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Role admin = roleService.getAdminRole();
				log.info("TESTCASE::MemberRoleServiceTest --> getAdminRoleTest -->> RESULT(ADMIN Role) =  " + admin);
				assertEquals(AppConstants.ROLE_ADMIN, admin.getRolename());
				return null;
			}
			
		});
		
	}

	@Test
	@InSequence(3)
	public void getMemberVerifiedRoleTest() throws Exception {
		log.info("TESTCASE::MemberRoleServiceTest --> getMemberVerifiedRoleTest");
		admin.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Role memberVerified = roleService.getMemberVerifiedRole();
				log.info("TESTCASE::MemberRoleServiceTest --> getMemberVerifiedRoleTest -->> RESULT(MEMBERVERIFIED Role) =  " + memberVerified);
				assertEquals(AppConstants.ROLE_MEMBER_VERIFIED, memberVerified.getRolename());
				return null;
			}
			
		});
	}

	@Test
	@InSequence(4)
	public void getMemberUnVerifiedRoleTest() {
		log.info("TESTCASE::MemberRoleServiceTest --> getMemberUnVerifiedRoleTest");
		Role memberUnVerified = roleService.getMemberUnVerifiedRole();
		log.info("TESTCASE::MemberRoleServiceTest --> getMemberUnVerifiedRoleTest -->> RESULT(MEMBERUNVERIFIED Role) =  " + memberUnVerified);
		assertEquals(AppConstants.ROLE_MEMBER_UNVERIFIED, memberUnVerified.getRolename());
	}

}
