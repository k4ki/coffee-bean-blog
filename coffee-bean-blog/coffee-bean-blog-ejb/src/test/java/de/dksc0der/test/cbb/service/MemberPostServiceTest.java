package de.dksc0der.test.cbb.service;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Callable;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.dksc0der.cbb.model.Category;
import de.dksc0der.cbb.model.Item;
import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.model.Post;
import de.dksc0der.cbb.repository.CategoryService;
import de.dksc0der.cbb.repository.MemberPostService;
import de.dksc0der.test.cbb.util.MemberVerified;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@RunWith(Arquillian.class)
public class MemberPostServiceTest {

	private static Logger log = Logger.getLogger(MemberPostService.class.getName());

	@EJB
	private MemberPostService postService;
	
	@EJB
	private CategoryService categoryService;
	
	@Inject
	private MemberVerified memberVerified;
	
	private static long id;
	
	@Deployment
	public static Archive<?> createTestArchive() {
		log.info("TESTCASE::MemberAccountServiceTest --> createTestArchive");
		return ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackages(true, "de.dksc0der.cbb")
				.addPackages(true, "de.dksc0der.test.cbb.util")
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsResource("import.sql", "import.sql").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				// Deploy test datasource
				.addAsWebInfResource("test-ds.xml", "test-ds.xml");
	}
	
	@Test
	@InSequence(1)
	public void create() throws Exception {
		log.info("TESTCASE::MemberPostService --> create");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				Member member = postService.findMemberByUsername("dummy");
				Post post = new Post();
				post.setTitle("NEUER TITEL");
				post.setDiscription("BESCHREIBUNG");
				
				post = postService.create(member, post);
				id = post.getId();
				log.info("TESTCASE::MemberPostService --> create -->> RESULT = " + post);
				assertNotNull(post);
				return member;
			}
		});
	}
	
	@Test
	@InSequence(2)
	public void updateCategory() throws Exception {
		log.info("TESTCASE::MemberPostService --> updateCategory");
		memberVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Category WCD = new Category();
				WCD.setCategoryname("WCD");
				Post p = new Post();
				p.setId(id);
				Post post = postService.updateCategory(p, WCD);
				log.info("TESTCASE::MemberPostService --> updateCategory -->> RESULT = " + post);
				assertNotNull(post);
				return null;
			}
		});
	}

	@Test
	@InSequence(3)
	public void deleteCategory() throws Exception {
		log.info("TESTCASE::MemberPostService --> deleteCategory");
		memberVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Category WCD = new Category();
				WCD.setCategoryname("WCD");
				Post p = new Post();
				p.setId(id);
				Post post = postService.deleteCategory(p, WCD);
				log.info("TESTCASE::MemberPostService --> deleteCategory -->> RESULT = " + post);
				assertNotNull(post);
				return null;
			}
		});
	}

	@Test
	@InSequence(4)
	public void updateItem() throws Exception {
		log.info("TESTCASE::MemberPostService --> updateItem");
		memberVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				String content = "Hier ist der Content";
				Item item = new Item();
				item.setContent(content);
				Post post = postService.findPostById(id);
				post = postService.updateItem(post, item);
				log.info("TESTCASE::MemberPostService --> updateItem -->> RESULT = " + post);
				assertEquals(1, post.getItems().size());
				return null;
			}
		});
	}

	@Test
	@InSequence(5)
	public void deleteItem() throws Exception {
		log.info("TESTCASE::MemberPostService --> deleteItem");
		memberVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Post post = postService.findPostById(id);
				Item item = null;
				Iterator<Item>itrItems = post.getItems().iterator();
				while (itrItems.hasNext()) {
					Item deleteItem = (Item) itrItems.next();
					if(deleteItem.getId() == 1)
						item = deleteItem;
				}
				
				post = postService.deleteItem(post, item);
				log.info("TESTCASE::MemberPostService --> deleteItem -->> RESULT = " + post);
				assertNotNull(post);
				return null;
			}
		});
	}

	@Test
	@InSequence(6)
	public void delete() throws Exception {
		log.info("TESTCASE::MemberPostService --> delete");
		memberVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Post post = postService.findPostById(id);
				postService.delete(post);
				post = postService.findPostById(id);
				log.info("TESTCASE::MemberPostService --> delete -->> RESULT = " + post);
				assertNull(post);
				return null;
			}
		});
	}

	@Test
	@InSequence(7)
	public void findPostById() throws Exception {
		log.info("TESTCASE::MemberPostService --> findPostById");
		memberVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Post post = postService.findPostById(1);
				log.info("TESTCASE::MemberPostService --> findPostById -->> RESULT = " + post);
				assertNotNull(post);
				return null;
			}
		});
	}

	@Test
	@InSequence(8)
	public void findAll() throws Exception {
		log.info("TESTCASE::MemberPostService --> findAll");
		memberVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Set<Post>posts = postService.findAll();
				log.info("TESTCASE::MemberPostService --> findAll -->> RESULT = " + posts);
				assertNotNull(posts);
				return null;
			}
		});
	}

	@Test
	@InSequence(9)
	public void findPostByCategory() throws Exception {
		log.info("TESTCASE::MemberPostService --> findPostByCategory");
		memberVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Category javaSE = new Category();
				javaSE.setCategoryname("JavaSE");
				Set<Post>posts = postService.findPostByCategory(javaSE);
				log.info("TESTCASE::MemberPostService --> findPostByCategory -->> RESULT = " + posts);
				assertNotNull(posts);
				return null;
			}
		});
	}

	@Test
	@InSequence(10)
	public void findPostByUsername() throws Exception {
		log.info("TESTCASE::MemberPostService --> findPostByUsername");
		memberVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Set<Post>posts = postService.findPostByUsername("dummy");
				log.info("TESTCASE::MemberPostService --> findPostByUsername -->> RESULT = " + posts);
				assertNotNull(posts);
				return null;
			}
		});
	}

	@Test
	@InSequence(11)
	public void findMemberByUsername() throws Exception {
		log.info("TESTCASE::MemberPostService --> findMemberByUsername");
		memberVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Member member = postService.findMemberByUsername("dummy");
				log.info("TESTCASE::MemberPostService --> findMemberByUsername -->> RESULT = " + member);
				assertNotNull(member);
				return null;
			}
		});
	}
}
