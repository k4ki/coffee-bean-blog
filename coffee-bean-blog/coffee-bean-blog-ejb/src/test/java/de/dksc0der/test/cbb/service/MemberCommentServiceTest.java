package de.dksc0der.test.cbb.service;

import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Callable;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.dksc0der.cbb.model.Comment;
import de.dksc0der.cbb.model.Item;
import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.model.Post;
import de.dksc0der.cbb.repository.MemberCommentsService;
import de.dksc0der.cbb.repository.MemberPostService;
import de.dksc0der.test.cbb.util.MemberVerified;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@RunWith(Arquillian.class)
public class MemberCommentServiceTest {

	private static Logger log = Logger.getLogger(MemberCommentServiceTest.class.getName());

	@EJB
	private MemberPostService postService;
	
	@EJB
	private MemberCommentsService commentService;
	
	@Inject
	private MemberVerified memberVerified;
	
	private static long id;
	
	@Deployment
	public static Archive<?> createTestArchive() {
		log.info("TESTCASE::MemberAccountServiceTest --> createTestArchive");
		return ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackages(true, "de.dksc0der.cbb")
				.addPackages(true, "de.dksc0der.test.cbb.util")
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsResource("import.sql", "import.sql").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				// Deploy test datasource
				.addAsWebInfResource("test-ds.xml", "test-ds.xml");
	}
	
	@Test
	@InSequence(1)
	public void create() throws Exception {
		log.info("TESTCASE::MemberCommentServiceTest --> create");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				Member member = postService.findMemberByUsername("dummy");
				Post post = postService.findPostById(1);
				
				Comment comment = new Comment();
				comment.setTitle("TEST TITEL!!!");
				comment.setDiscription("TEST DISCRIPTION!!!");
				
				comment = commentService.create(member, post, comment);
				id = comment.getId();
				log.info("TESTCASE::MemberCommentServiceTest --> create -->> RESULT = " + comment);
				assertNotNull(comment);
				return null;
			}
		});
	}
	
	@Test
	@InSequence(9)
	public void delete() throws Exception {
		log.info("TESTCASE::MemberCommentServiceTest --> delete");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				Comment comment = commentService.findCommentById(id);
				commentService.delete(comment);
				comment = commentService.findCommentById(id);
				log.info("TESTCASE::MemberCommentServiceTest --> delete -->> RESULT = " + comment);
				assertNull(comment);
				return null;
			}
		});
	}

	@Test
	@InSequence(2)
	public void updateItem() throws Exception {
		log.info("TESTCASE::MemberCommentServiceTest --> updateItem");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				String content = "Hier ist der Content";
				Item item = new Item();
				item.setContent(content);
				Comment comment = commentService.findCommentById(id);
				comment = commentService.updateItem(comment, item);
				log.info("TESTCASE::MemberCommentServiceTest --> updateItem -->> RESULT = " + comment);
				assertEquals(1, comment.getItems().size());
				return null;
			}
		});
	}

	@Test
	@InSequence(3)
	public void deleteItem() throws Exception {
		log.info("TESTCASE::MemberCommentServiceTest --> deleteItem");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				Comment comment = commentService.findCommentById(id);
				Item item = null;
				Iterator<Item>itrItems = comment.getItems().iterator();
				while (itrItems.hasNext()) {
					Item deleteItem = (Item) itrItems.next();
					if(deleteItem.getId() == 1)
						item = deleteItem;
				}
				
				comment = commentService.deleteItem(comment, item);
				log.info("TESTCASE::MemberCommentServiceTest --> deleteItem -->> RESULT = " + comment);
				assertNotNull(comment);
				return null;
			}
		});
	}

	@Test
	@InSequence(4)
	public void findCommentById() throws Exception {
		log.info("TESTCASE::MemberCommentServiceTest --> findCommentById");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				Comment comment = commentService.findCommentById(1);
				log.info("TESTCASE::MemberCommentServiceTest --> findCommentById -->> RESULT = " + comment);
				assertNotNull(comment);
				return null;
			}
		});
	}
	@Test
	@InSequence(5)
	public void findCommentsByUsername() throws Exception {
		log.info("TESTCASE::MemberCommentServiceTest --> findCommentsByUsername");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				Set<Comment> comments = commentService.findCommentsByUsername("dummy");
				log.info("TESTCASE::MemberCommentServiceTest --> findCommentsByUsername -->> RESULT = " + comments);
				assertNotNull(comments);
				return null;
			}
		});
	}

	@Test
	@InSequence(6)
	public void findCommentByPost() throws Exception {
		log.info("TESTCASE::MemberCommentServiceTest --> findCommentByPost");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				Post post = new Post();
				post.setId(1);
				Set<Comment> comments = commentService.findCommentByPost(post);
				log.info("TESTCASE::MemberCommentServiceTest --> findCommentsByUsername -->> RESULT = " + comments);
				assertNotNull(comments);
				return null;
			}
		});
	}

	@Test
	@InSequence(7)
	public void findPostByID() throws Exception {
		log.info("TESTCASE::MemberCommentServiceTest --> findPostByID");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				Post post = commentService.findPostByID(1);
				log.info("TESTCASE::MemberCommentServiceTest --> findCommentsByUsername -->> RESULT = " + post);
				assertNotNull(post);
				return null;
			}
		});
	}

	@Test
	@InSequence(8)
	public void findMemberByUsername() throws Exception {
		log.info("TESTCASE::MemberCommentServiceTest --> findMemberByUsername");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				Member member = postService.findMemberByUsername("dummy");
				log.info("TESTCASE::MemberCommentServiceTest --> findMemberByUsername -->> RESULT = " + member);
				assertNotNull(member);
				return null;
			}
		});
	}

}
