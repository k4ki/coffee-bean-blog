package de.dksc0der.test.cbb.service;

import static org.junit.Assert.*;

import java.util.concurrent.Callable;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.repository.MemberSecurityService;
import de.dksc0der.test.cbb.util.MemberUnVerified;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@RunWith(Arquillian.class)
public class MemberSecurityServiceTest {

	private static Logger log = Logger.getLogger(MemberSecurityServiceTest.class.getName());

	@EJB
	private MemberSecurityService securityService;
	
	@Inject
	private MemberUnVerified unVerified;
	
	private static String TOKEN = null; 

	@Deployment
	public static Archive<?> createTestArchive() {
		log.info("TESTCASE::MemberRoleServiceTest --> createTestArchive");
		return ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackages(true, "de.dksc0der.cbb")
				.addPackages(true, "de.dksc0der.test.cbb.util")
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsResource("import.sql", "import.sql").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				// Deploy test datasource
				.addAsWebInfResource("test-ds.xml", "test-ds.xml");
	}
	
	/**
	 * 	@throws Exception 
	 * @Override
	 *	public Member beginSession(Member member) {
	 *		return null;
	 *	}
	 */
	@Test
	@InSequence(1)
	public void beginSessionTest() throws Exception {
		log.info("TESTCASE::MemberSecurityServiceTest --> beginSessionTest");
		unVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Member member = securityService.findByUsername("dummy");
				member = securityService.beginSession(member);
				log.info("TESTCASE::MemberSecurityServiceTest --> beginSessionTest -->> RESULT = \n" + member);
				assertTrue(member.isActive());
				return null;
			}
		});
	}

	/**
	 * 	@throws Exception 
	 * @Override
	 *	public Member endSession(Member member) {
	 *		return null;
	 *	}
	 */
	@Test
	@InSequence(2)
	public void endSessionTest() throws Exception {
		log.info("TESTCASE::MemberSecurityServiceTest --> endSessionTest");
		unVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Member member = securityService.findByUsername("dummy");
				member = securityService.endSession(member);
				log.info("TESTCASE::MemberSecurityServiceTest --> endSessionTest -->> RESULT = \n" + member);
				assertFalse(member.isActive());
				return null;
			}
		});
	}

	/**
	 * 	@throws Exception 
	 * @Override
	 *	public String beginVerification(Member member) {
	 *		return null;
	 *	}
	 */
	@Test
	@InSequence(3)
	public void beginVerificationTest() throws Exception {
		log.info("TESTCASE::MemberSecurityServiceTest --> beginVerificationTest");
		unVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Member member = securityService.findByUsername("dummy");
				member = securityService.beginSession(member);
				TOKEN = securityService.beginVerification(member);
				member = securityService.findByUsername("dummy");
				log.info("TESTCASE::MemberSecurityServiceTest --> beginVerificationTest -->> RESULT = \n" + TOKEN + "\n" + member);
				assertNotNull(TOKEN);
				return null;
			}
		});
	}

	/**
	 * 	@throws Exception 
	 * @Override
	 *	public Member endVerification(String token) {
	 *		return null;
	 *	}
	 */
	@Test
	@InSequence(4)
	public void endVerificationTest() throws Exception {
		log.info("TESTCASE::MemberSecurityServiceTest --> endVerificationTest");
		unVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Member member = securityService.endVerification(TOKEN);
				member = securityService.endSession(member);
				log.info("TESTCASE::MemberSecurityServiceTest --> endVerificationTest -->> RESULT = \n" + member);
				assertNotNull(member);
				
				return null;
			}
		});
	}

	/**
	 * 	@throws Exception 
	 * @Override
	 *	public String passwordReset(String email) {
	 *		return null;
	 *	}
	 */
	@Test
	@InSequence(5)
	public void passwordResetTest() throws Exception {
		log.info("TESTCASE::MemberSecurityServiceTest --> passwordResetTest");
		
		TOKEN = securityService.passwordReset("davkun107@live.de");
		Member member = securityService.findByUsername("dummy");
		log.info("TESTCASE::MemberSecurityServiceTest --> passwordResetTest -->> RESULT = \n" + TOKEN + "\n" + member);
		assertNotNull(TOKEN);
		
	}

	/**
	 * 	@throws Exception 
	 * @Override
	 *	public Member passwordUpdate(String checksum, String password) {
	 *		return null;
	 *	}
	 */
	@Test
	@InSequence(6)
	public void passwordUpdateTest() throws Exception {
		log.info("TESTCASE::MemberSecurityServiceTest --> passwordUpdateTest");
		Member member = securityService.passwordUpdate(TOKEN, "12345678");
		log.info("TESTCASE::MemberSecurityServiceTest --> passwordUpdateTest -->> RESULT = \n" + "\n" + member);
		assertNotNull(TOKEN);
	}

	/**
	 * 	@throws Exception 
	 * @Override
	 *	public Member emailUpdate(Member member, String email) {
	 *		return null;
	 *	}
	 */
	@Test
	@InSequence(7)
	public void emailUpdateTest() throws Exception {
		log.info("TESTCASE::MemberSecurityServiceTest --> emailUpdateTest");
		unVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				return null;
			}
		});
	}

	/**
	 * 	@throws Exception 
	 * @Override
	 *	public Member findByUsername(String username) {
	 *		return null;
	 *	}
	 */
	@Test
	@InSequence(8)
	public void findByUsernameTest() throws Exception {
		log.info("TESTCASE::MemberSecurityServiceTest --> findByUsernameTest");
		unVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Member member = securityService.findByUsername("dummy");
				log.info("TESTCASE::MemberSecurityServiceTest --> findByUsernameTest -->> RESULT = " + member);
				assertNotNull(member);
				return null;
			}
		});
	}

	/**
	 * 	@throws Exception 
	 * @Override
	 *	public Member findByEmail(String email) {
	 *		return null;
	 *	}
	 */
	@Test
	@InSequence(9)
	public void findByEmailTest() throws Exception {
		log.info("TESTCASE::MemberSecurityServiceTest --> findByEmailTest");
		unVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Member find = securityService.findByEmail("zus3iik4k@gmail.com");
				log.info("TESTCASE::MemberSecurityServiceTest --> findByEmailTest -->> RESULT(findMember) =  \n" + find);
				assertNotNull(find);
				return null;
			}
		});
	}

	/**
	 * 	@throws Exception 
	 * @Override
	 *	public Member findByToken(String username, String email, Timestamp createdChecksum, String checksum) {
	 *		return null;
	 *	}
	 */
	@Test
	@InSequence(10)
	public void findByTokenTest() throws Exception {
		log.info("TESTCASE::MemberSecurityServiceTest --> findByTokenTest");
		unVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				Member member = securityService.findByUsername("dummy");
				securityService.beginVerification(member);
				member = securityService.findByUsername("dummy");
				
				Member find = securityService.findByToken(member.getUsername(), member.getEmail(), member.getCreatedChecksum(), member.getChecksum());
				
				log.info("TESTCASE::MemberSecurityServiceTest --> findByTokenTest -->> RESULT(findMember) =  \n" + find);
				assertNotNull(find);
				return null;
			}
		});
	}

	/**
	 * 	@throws Exception 
	 * @Override
	 *	public long countAllMember() {
	 *		return null;
	 *	}
	 */
	@Test
	@InSequence(11)
	public void countAllMemberTest() throws Exception {
		log.info("TESTCASE::MemberSecurityServiceTest --> countAllMemberTest");
		unVerified.call(new Callable<Member>() {

			@Override
			public Member call() throws Exception {
				long count = securityService.countAllMember();
				log.info("TESTCASE::MemberSecurityServiceTest --> countAllMemberTest -->> RESULT(countMember) =  " + count);
				assertNotEquals(0, count);
				return null;
			}
		});
	}

}
