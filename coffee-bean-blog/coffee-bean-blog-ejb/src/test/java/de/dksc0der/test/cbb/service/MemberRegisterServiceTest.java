package de.dksc0der.test.cbb.service;

import static org.junit.Assert.*;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.repository.MemberRegisterService;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@RunWith(Arquillian.class)
public class MemberRegisterServiceTest {

	private static Logger log = Logger.getLogger(MemberRegisterServiceTest.class.getName());

	@EJB
	private MemberRegisterService registerService;
	
	@Deployment
	public static Archive<?> createTestArchive() {
		log.info("TESTCASE::MemberRegisterServiceTest --> createTestArchive");
		return ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackages(true, "de.dksc0der.cbb")
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsResource("import.sql", "import.sql").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				// Deploy test datasource
				.addAsWebInfResource("test-ds.xml", "test-ds.xml");
	}
	
	@Test
	@InSequence(1)
	public void saveAndEncodeTest() {
		log.info("TESTCASE::MemberRegisterServiceTest --> saveAndEncodeTest");
		Member m = new Member();
		m.setUsername("begbie");
		m.setEmail("begbie@bbb.bb");
		m.setPassword("12345678");
		Member rm = registerService.saveAndEncode(m);
		log.info("TESTCASE::MemberRegisterServiceTest --> saveAndEncodeTest -->> RESULT =  " + rm);
		assertNotNull(rm);
	}
	
	@Test
	@InSequence(2)
	public void countAllMemberTest() {
		log.info("TESTCASE::MemberRegisterServiceTest --> countAllMemberTest");
		long count = registerService.countAllMember();
		log.info("TESTCASE::MemberRegisterServiceTest --> countAllMemberTest -->> RESULT(countMember) =  " + count);
		assertNotEquals(0, count);
	}
	
}
