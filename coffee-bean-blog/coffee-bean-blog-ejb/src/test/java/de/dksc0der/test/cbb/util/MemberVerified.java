package de.dksc0der.test.cbb.util;

import java.util.concurrent.Callable;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RunAs;
import javax.ejb.Stateless;

import de.dksc0der.cbb.util.AppConstants;


@RunAs(AppConstants.ROLE_MEMBER_VERIFIED)
@Stateless
@PermitAll
public class MemberVerified {
	public <V>V call(Callable<V> callable)throws Exception{
		return callable.call();
	}
}
