package de.dksc0der.test.cbb.service;

import static org.junit.Assert.*;

import java.util.concurrent.Callable;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.dksc0der.cbb.model.Category;
import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.repository.MemberAccountService;
import de.dksc0der.test.cbb.util.MemberVerified;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@RunWith(Arquillian.class)
public class MemberAccountServiceTest{

	private static Logger log = Logger.getLogger(MemberAccountServiceTest.class.getName());

	@EJB
	private MemberAccountService accountService;
	
	@Inject
	private MemberVerified memberVerified;
	
	@Deployment
	public static Archive<?> createTestArchive() {
		log.info("TESTCASE::MemberAccountServiceTest --> createTestArchive");
		return ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackages(true, "de.dksc0der.cbb")
				.addPackages(true, "de.dksc0der.test.cbb.util")
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsResource("import.sql", "import.sql").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				// Deploy test datasource
				.addAsWebInfResource("test-ds.xml", "test-ds.xml");
	}
	
	@Test
	@InSequence(1)
	public void findByUsernameTest() throws Exception {
		log.info("TESTCASE::MemberAccountServiceTest --> findByUsernameTest");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				Member member = accountService.findByUsername("dksc0der");
				log.info("TESTCASE::MemberAccountServiceTest --> findByUsernameTest -->> RESULT = " + member);
				assertNotNull(member);
				return null;
			}
		});
		
	}

	@Test
	@InSequence(2)
	public void updateSecurityTest() throws Exception {
		log.info("TESTCASE::MemberAccountServiceTest --> updateSecurityTest");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				Member member = accountService.findByUsername("dksc0der");
				member.setEmail("blo@blo.bb");
				
				Member checkMember = accountService.updateSecurity(member);
				log.info("TESTCASE::MemberAccountServiceTest --> updateSecurityTest -->> RESULT(email = blo@blo.bb) =  " + checkMember);
				assertEquals(member.getEmail(), checkMember.getEmail());
				return null;
			}
		});
	}

	@Test
	@InSequence(3)
	public void updateMemberInfoTest() throws Exception {
		log.info("TESTCASE::MemberAccountServiceTest --> updateMemberInfoTest");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				
				Member member = accountService.findByUsername("dksc0der");
				member.setFirstname("Donald");
				
				Member checkMember = accountService.updateMemberInfo(member);
				log.info("TESTCASE::MemberAccountServiceTest --> updateSecurityTest -->> RESULT(firstname = Donald) =  " + checkMember);
				assertEquals(member.getFirstname(), checkMember.getFirstname());
				return null;
			}
		});
	}
	
	@Test
	@InSequence(4)
	public void updateMemberCategoriesTest() throws Exception {
		log.info("TESTCASE::MemberAccountServiceTest --> updateMemberCategoriesTest");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				
				Category jvm = new Category();
				jvm.setCategoryname("JavaME");
				Category ejb = new Category();
				ejb.setCategoryname("EJB");
				
				
				
				Member member = accountService.findByUsername("dksc0der");
				member = accountService.updateMemberCategory(member, ejb);
				member = accountService.updateMemberCategory(member, jvm);
				
				log.info("TESTCASE::MemberAccountServiceTest --> updateSecurityTest -->> RESULT(CategoriesMember) =  " + member);
				
				assertNotNull(member);
				return null;
			}
		});
	}
	
	@Test
	@InSequence(5)
	public void deleteMemberCategory() throws Exception {
		log.info("TESTCASE::MemberAccountServiceTest --> deleteMemberCategory");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				
				Category jvm = new Category();
				jvm.setCategoryname("JavaME");
				
				
				
				Member member = accountService.findByUsername("dksc0der");
				member = accountService.deleteMemberCategory(member, jvm);
				log.info("TESTCASE::MemberAccountServiceTest --> deleteMemberCategory -->> RESULT(CategoriesMember) =  " + member);
				assertNotNull(member);
				return null;
			}
		});
	}

	@Test
	@InSequence(6)
	public void updatePasswordTest() throws Exception {
		log.info("TESTCASE::MemberAccountServiceTest --> updatePasswordTest");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				Member member = accountService.findByUsername("dksc0der");
				String old = member.getPassword();
				member.setPassword("12345678");
				
				Member checkMember = accountService.updatePassword(member);
				log.info("TESTCASE::MemberAccountServiceTest --> updateSecurityTest -->> RESULT(Password) =  " + checkMember);
				assertNotEquals(old, checkMember.getPassword());
				return null;
			}
		});
	}

	@Test
	@InSequence(7)
	public void updateAvatarTest() throws Exception {
		log.info("TESTCASE::MemberAccountServiceTest --> updateAvatarTest");
		memberVerified.call(new Callable<Member>() {
			@Override
			public Member call() throws Exception {
				Member member = accountService.findByUsername("dksc0der");
				member.setAvatarpath("newImage.png");
				
				Member checkMember = accountService.updateAvatar(member);
				log.info("TESTCASE::MemberAccountServiceTest --> updateSecurityTest -->> RESULT(avatar = /newImage) =  " + checkMember);
				assertEquals(member.getAvatarpath(), checkMember.getAvatarpath());
				return null;
			}
		});
	}
}
