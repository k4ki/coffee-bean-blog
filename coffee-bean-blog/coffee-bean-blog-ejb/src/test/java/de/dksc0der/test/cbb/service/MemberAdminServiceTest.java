package de.dksc0der.test.cbb.service;

import java.util.Set;
import java.util.concurrent.Callable;

import javax.ejb.EJB;
import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.logging.Logger;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import de.dksc0der.cbb.model.Member;
import de.dksc0der.cbb.model.Role;
import de.dksc0der.cbb.repository.MemberAdminService;
import de.dksc0der.cbb.util.AppConstants;
import de.dksc0der.test.cbb.util.Admin;

/**
 * 
 * @author David Kunschke alias dksc0der
 *
 */
@RunWith(Arquillian.class)
public class MemberAdminServiceTest {

	private static Logger log = Logger.getLogger(MemberAdminServiceTest.class.getName());

	@EJB
	private MemberAdminService adminService;
	
	@Inject
	private Admin admin;
	
	@Deployment
	public static Archive<?> createTestArchive() {
		log.info("TESTCASE::MemberAccountServiceTest --> createTestArchive");
		return ShrinkWrap.create(WebArchive.class, "test.war")
				.addPackages(true, "de.dksc0der.cbb")
				.addPackages(true, "de.dksc0der.test.cbb.util")
				.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
				.addAsResource("import.sql", "import.sql").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				// Deploy test datasource
				.addAsWebInfResource("test-ds.xml", "test-ds.xml");
	}
	
	@Test
	@InSequence(1)
	public void promote() throws Exception {
		log.info("TESTCASE::MemberAdminServiceTest --> promote");
		admin.call(new Callable<Member>(){
			@Override
			public Member call() throws Exception {
				Member member = adminService.findByUsername("dummy");
				Role role = new Role();
				role.setRolename(AppConstants.ROLE_ADMIN);
				member = adminService.promote(member, role);
				log.info("TESTCASE::MemberAdminServiceTest --> promote -->> RESULT = " + member);
				return null;
			}
			
		});
	}

	@Test
	@InSequence(2)
	public void demote() throws Exception {
		log.info("TESTCASE::MemberAdminServiceTest --> demote");
		admin.call(new Callable<Member>(){
			@Override
			public Member call() throws Exception {
				Member member = adminService.findByUsername("dummy");
				Role role = new Role();
				role.setRolename(AppConstants.ROLE_ADMIN);
				member = adminService.demote(member, role);
				log.info("TESTCASE::MemberAdminServiceTest --> demote -->> RESULT = " + member);
				return null;
			}
			
		});
	}

	@Test
	@InSequence(3)
	public void findByUsername() throws Exception {
		log.info("TESTCASE::MemberAdminServiceTest --> findByUsername");
		admin.call(new Callable<Member>(){
			@Override
			public Member call() throws Exception {
				Member member = adminService.findByUsername("dummy");
				log.info("TESTCASE::MemberAdminServiceTest --> findAll -->> RESULT = " + member);
				assertEquals("dummy", member.getUsername());
				return null;
			}
			
		});
	}

	@Test
	@InSequence(4)
	public void findAll() throws Exception {
		log.info("TESTCASE::MemberAdminServiceTest --> findAll");
		admin.call(new Callable<Member>(){
			@Override
			public Member call() throws Exception {
				Set<Member> all = adminService.findAll();
				log.info("TESTCASE::MemberAdminServiceTest --> findAll -->> RESULT = " + all);
				assertNotNull(all);
				return null;
			}
			
		});
	}

}
